package com.flex.agentdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import static com.flex.agentdb.PkgMgmtContract.ALL_COLUMN_NAMES;
import static com.flex.agentdb.PkgMgmtContract.PkgMgmtEntry;
import static com.flex.agentdb.PkgMgmtContract.SQL_EQ_PKG_NAME;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Package management database API functions
 * <p>
 * Created by Alex.Molina@flex.com
 */

public class PkgMgmtDb {
    public static final String LOG_TAG = "AGENT_PKG_DB";

    private SQLiteDatabase mDb = null;


    /**
     * Constructor
     *
     * @param context Application context for databases
     */
    PkgMgmtDb(Context context) {
        // throws NullPointerException if fails
        PkgMgmtDbHelper dbHelper = PkgMgmtDbHelper.getPkgMgmtDbHelper(context);
        mDb = dbHelper.open();
    }

    /**
     * Inserts new Package entry
     *
     * @param listPkgJobj JSON object of a single Intel ListPackages package entry
     * @return PkgMgmt database row _ID
     */

    public long insert(final JSONObject listPkgJobj) {
        long rowId = -1;

        try {
            String pkgName = getPkgName(listPkgJobj);

            if ((pkgName != null) && !pkgName.isEmpty()) {
                rowId = findByPkgName(pkgName);

                // insert if pkgName not in database
                if (rowId == -1) {
                    Log.d(LOG_TAG, "insert new pkgName: " + pkgName);

                    ContentValues row = PkgMgmtContract.initRow();

                    if (listPkgJobj != null) {
                        setRowFromListPkg(listPkgJobj, row);
                    }

                    rowId = mDb.insert(PkgMgmtEntry.TABLE_NAME, null, row);
                } else {
                    Log.d(LOG_TAG, "DB already has pkgName: " + pkgName);
                }
            }
        } catch (Exception error) {
            Log.e(LOG_TAG, "insert failed: " + error.toString());
        }

        return rowId;
    }

    /**
     * @param listPkgJobj JSON object of a single ListPackages entry, may be null
     * @return database row _ID or -1 on not found
     */
    public int update(final JSONObject listPkgJobj) {

        int rowId = -1;
        String pkgName = null;

        try {
            ContentValues row = PkgMgmtContract.initRow();
            pkgName = getPkgName(listPkgJobj);

            if ((pkgName != null) && !pkgName.isEmpty()) {

                row.put(PkgMgmtEntry.COLUMN_NAME_PKG_NAME, pkgName);

                if (listPkgJobj != null) {
                    setRowFromListPkg(listPkgJobj, row);
                }

                String[] selectionArgs = {pkgName};

                rowId = mDb.update(PkgMgmtEntry.TABLE_NAME,
                        row,
                        SQL_EQ_PKG_NAME,
                        selectionArgs);
            }
        } catch (Exception err) {
            Log.e(LOG_TAG, "update failed: " + err.toString());
        }

        return rowId;
    }

    public String getPkgName(JSONObject listPkgJobj)
            throws JSONException {
        String pkgName = null;

        if (listPkgJobj != null) {
            pkgName = listPkgJobj.getString((PkgMgmtEntry.COLUMN_NAME_PKG_NAME));
        }
        return pkgName;
    }

    /**
     * sets columnn values from a single Intel ListPackages entry
     *
     * @param listPkgJobj JSON of Intel ListPackages entry
     * @param row         row columns to be set
     */
    public void setRowFromListPkg(JSONObject listPkgJobj, ContentValues row)
            throws JSONException {

        if ((listPkgJobj != null) && (row != null)) {
            try {
                row.put(PkgMgmtEntry.COLUMN_NAME_PKG_NAME,
                        listPkgJobj.getString(PkgMgmtEntry.COLUMN_NAME_PKG_NAME));

                row.put(PkgMgmtEntry.COLUMN_NAME_PKG_VERSION,
                        listPkgJobj.getString(PkgMgmtEntry.COLUMN_NAME_PKG_VERSION));

                row.put(PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE,
                        listPkgJobj.getString(PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE));

                row.put(PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING,
                        listPkgJobj.getBoolean(PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING));

                row.put(PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG,
                        listPkgJobj.getBoolean(PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG));


            } catch (JSONException jsonError) {
                Log.e(LOG_TAG, jsonError.toString());
                throw jsonError;
            }

        }
    }

    public JSONObject getRowbyPkgName(final String pkgName)
            throws JSONException {
        final String[] selectionArgs = {pkgName};

        Cursor cursor = mDb.query(PkgMgmtEntry.TABLE_NAME,
                ALL_COLUMN_NAMES,
                SQL_EQ_PKG_NAME, selectionArgs, null, null, null, "1");

        JSONObject jobj = new JSONObject();
        // if  Android package name found
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    // set entire row
                    copyColsJson(cursor, jobj);
                }
            } finally {
                // free resources
                cursor.close();
            }
        }

        return jobj;

    }

    /**
     * Copy column values from SQL row to JSON object
     * Copies String(Text) and integers, other types replaced by empty string
     *
     * @param cursor SQL row to copy values from
     * @param jobj   JSON object to copy values into
     * @throws JSONException
     */
    private void copyColsJson(Cursor cursor, JSONObject jobj)
            throws JSONException {
        int i = 0;
        try {
            // set entire row
            for (i = 0; i < cursor.getColumnCount(); i++) {
                int field_type = cursor.getType(i);

                if (field_type == Cursor.FIELD_TYPE_STRING) {
                    jobj.put(ALL_COLUMN_NAMES[i], cursor.getString(i));
                } else if (field_type == Cursor.FIELD_TYPE_INTEGER) {
                    jobj.put(ALL_COLUMN_NAMES[i], cursor.getInt(i));
                } else {
                    jobj.put(ALL_COLUMN_NAMES[i], "");
                }
            }
        } catch (JSONException jsonErr) {
            Log.e(LOG_TAG, "JSON failure on " + ALL_COLUMN_NAMES[i]);
            throw jsonErr;
        }
    }


    /**
     * @param pkgName Android package name
     * @return number of package management entries deleted or -1 if not found/error
     */
    public int deleteByPkg(final String pkgName) {
        String[] selectionArgs = {pkgName};

        return mDb.delete(PkgMgmtEntry.TABLE_NAME,
                PkgMgmtContract.SQL_EQ_PKG_NAME,
                selectionArgs);
    }


    /**
     * @param pkgName Android package name
     * @return PkgMgmtEntry database row _ID
     */
    public int findByPkgName(final String pkgName) {
        final String[] columns = {PkgMgmtEntry._ID};
        final String[] selectionArgs = {pkgName};

        Cursor cursor = mDb.query(PkgMgmtEntry.TABLE_NAME, columns,
                PkgMgmtContract.SQL_EQ_PKG_NAME, selectionArgs, null, null, null, "1");

        int rowId = -1;
        // if Android package name found
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                // return row _ID which is column index 0
                rowId = cursor.getInt(0);
            }
            // free resources
            cursor.close();
        }
        return rowId;
    }


    /**
     * @return number of Android package entries
     */
    public int getNumRows() {
        final String[] columns = {PkgMgmtEntry._ID};
        Cursor cursor = mDb.query(PkgMgmtEntry.TABLE_NAME, columns,
                null, null, null, null, null, null);

        int numRows = 0;
        if (cursor != null) {
            numRows = cursor.getCount();
            cursor.close();
        }
        return numRows;
    }
}
