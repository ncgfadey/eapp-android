package com.flex.agentdb;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Package management database open framework callbacks
 * <p>
 * Created by Alex.Molina@flex.com
 */

public class PkgMgmtDbHelper extends SQLiteOpenHelper {

    private static PkgMgmtDbHelper mPkgMgmtDbHelper = null;
    private static SQLiteDatabase mDb = null;
    private static Context mContext = null;

    public static final String LOG_TAG = "AGENT_HELPER_DB";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "agent_pkg.db";


    /*
     * private constructor for singleton
     */
    private PkgMgmtDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * @param context Application context for database
     * @return singleton PkgMgmtDbHelper object
     */
    public static synchronized PkgMgmtDbHelper getPkgMgmtDbHelper(Context context)
            throws NullPointerException {
        if (mPkgMgmtDbHelper == null) {
            mContext = context;
            mPkgMgmtDbHelper = new PkgMgmtDbHelper(context);
        }
        if (mPkgMgmtDbHelper == null) {
            throw new NullPointerException("PkgMgmtDbHelper singleton not created");
        }
        return mPkgMgmtDbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Intel ListPackages table
        Log.d(LOG_TAG, PkgMgmtContract.SQL_CREATE_ENTRIES);
        db.execSQL(PkgMgmtContract.SQL_CREATE_ENTRIES);

        Log.d(LOG_TAG, PkgMgmtContract.SQL_CREATE_PKG_INDEX);
        db.execSQL(PkgMgmtContract.SQL_CREATE_PKG_INDEX);

        Log.d(LOG_TAG, ImageMgmtContract.SQL_CREATE_ENTRIES);
        db.execSQL(ImageMgmtContract.SQL_CREATE_ENTRIES);

        Log.d(LOG_TAG, ImageMgmtContract.SQL_CREATE_IMAGE_INDEX);
        db.execSQL(ImageMgmtContract.SQL_CREATE_IMAGE_INDEX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(PkgMgmtContract.SQL_DELETE_ENTRIES);
        db.execSQL(ImageMgmtContract.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * open the DB with write/read access or
     * just read access if that is all that is possible.
     *
     * @return this  Package management database adapter
     * @throws SQLException
     */
    public synchronized SQLiteDatabase open() throws SQLException {
        Log.d(LOG_TAG, "open() " + PkgMgmtDbHelper.DATABASE_NAME);
        if (mDb == null) {
            try {
                mDb = getWritableDatabase();
            } catch (SQLException ex) {
                mDb = getReadableDatabase();
            }
        }
        return mDb;
    }

    /**
     * close the DB.
     */
    public synchronized void close() {
        Log.d(LOG_TAG, "close() " + PkgMgmtDbHelper.DATABASE_NAME);
        if (mDb != null) {
            mDb.close();
            mDb = null;
        }
    }

    /**
     * deletes Package management database file
     */
    public boolean deleteDbFile() {
        boolean bSuccessful = false;

        if (mContext != null) {
            bSuccessful = deleteDbFile(mContext);
        }
        return bSuccessful;
    }

    public static synchronized boolean deleteDbFile(Context context) {
        boolean bSuccessful = false;

        if (context != null) {
            bSuccessful = context.deleteDatabase(PkgMgmtDbHelper.DATABASE_NAME);
            if (bSuccessful) {
                Log.d(LOG_TAG, "Successfully deleted: " + PkgMgmtDbHelper.DATABASE_NAME);
            } else {
                Log.e(LOG_TAG, "Failed to delete: " + PkgMgmtDbHelper.DATABASE_NAME);
            }
        }
        return bSuccessful;
    }


    /**
     * Get the underlying Database.
     *
     * @return
     */
    public SQLiteDatabase getDB() {
        return mDb;
    }

}
