package com.flex.agentdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/
/**
 * BT Device database management functions
 *
 * Created by alexm on 7/7/17.
 */

public class BtDeviceDb {
    public static final String LOG_TAG =  "BT_DEVICE_DB";

    // cache application context
    private final Context mContext;
    private final BtDeviceDbHelper mDbHelper;
    private SQLiteDatabase mDb = null;

    /**
     * Constructor
     *   automatically opens writable database
     *
     * @param context  normally Application context
     */
    public BtDeviceDb(Context context) {
        mContext = context;
        mDbHelper = new BtDeviceDbHelper(context);
    }

    /**
     * Inserts new BT device
     *
     * @param bt_mac  MAC address of BT device
     * @param bt_name  Model name of BT device
     * @return  BT device database row _ID
     */

    public synchronized long insert(final int epId, final String bt_mac, final String bt_name) {

        long rowId = findByMac(bt_mac);

        // insert if BT MAC not in database
        if ( rowId == -1 ) {
            Log.d(LOG_TAG, "insert new BT MAC: " + bt_mac);

            ContentValues row = new ContentValues();

            row.put(BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_MAC, bt_mac);
            row.put(BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_NAME, bt_name);
            row.put(BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_LOCAL_ID, epId);

            rowId = mDb.insert(BtDeviceContract.BtDeviceEntry.TABLE_NAME, null, row);
        }
        else {
            Log.d(LOG_TAG, "DB already has BT MAC: " + bt_mac);
        }

        return rowId;
    }

    /**
     *
     * @param bt_mac  MAC address of BT device
     * @return number of BT devices deleted or -1 if not found/error
     */
    public synchronized int deleteByMac(final String bt_mac) {
        String[] selectionArgs = { bt_mac };

        return mDb.delete(BtDeviceContract.BtDeviceEntry.TABLE_NAME,
                BtDeviceContract.SQL_EQ_BT_MAC,
                selectionArgs);
    }

    /**
     * deletes BT device given endpoint local id
     *
     * @param localId  endpoint local id
     * @return
     */
    public synchronized int deleteByLocalId(final int localId) {
        String[] selectionArgs = { Integer.toString(localId) };

        return mDb.delete(BtDeviceContract.BtDeviceEntry.TABLE_NAME,
                BtDeviceContract.SQL_EQ_LOCAL_ID,
                selectionArgs);

    }

    /**
     * lookup by local endpoint id for BT MAC address
     *
     * @param localId
     * @return  String BT MAC address
     */
    public String getBtMacForLocalId(final int localId) {
        final String[] columns = { BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_MAC};
        final String[] selectionArgs = { Integer.toString(localId) };

        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                BtDeviceContract.SQL_EQ_LOCAL_ID, selectionArgs, null, null, null, "1");

        String bt_mac = null;
        // if BT_MAC found
        if ( cursor != null ) {

            if ( cursor.moveToFirst() ) {
                // return bt_mac in column 0 of result set
                bt_mac = cursor.getString(0);
            }

            // free resources
            cursor.close();
        }

        return bt_mac;
    }


    /**
     * lookup by BT MAC address for local id endpoint
     *
     * @param bt_mac
     * @return  String BT MAC address
     */
    public int getLocalIdforBtMac(final String bt_mac) {
        final String[] columns = { BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_LOCAL_ID};
        final String[] selectionArgs = { bt_mac };

        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                BtDeviceContract.SQL_EQ_BT_MAC, selectionArgs, null, null, null, "1");

        int localId = -1;
        // if BT_MAC found
        if ( cursor != null ) {

            if ( cursor.moveToFirst() ) {
                // return endpoint local id in column 0 of result set
                localId = cursor.getInt(0);
            }

            // free resources
            cursor.close();
        }

        return localId;
    }

    /**
     *
     * @param bt_mac  MAC address of BT device
     * @return  BT device database row _ID
     */
    public int findByMac(final String bt_mac) {
        final String[] columns = { BtDeviceContract.BtDeviceEntry._ID };
        final String[] selectionArgs = { bt_mac };

        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                BtDeviceContract.SQL_EQ_BT_MAC, selectionArgs, null, null, null, "1");

        int rowId = -1;
        // if BT_MAC found
        if ( cursor != null ) {

            if ( cursor.moveToFirst() ) {
                // return row _ID which is column index 0
                rowId = cursor.getInt(0);
            }
            // free resources
            cursor.close();
        }
        return rowId;
    }

    /**
     *
     * @param localId  endpoint local id
     * @return BT device database row _ID
     */
    public int findByLocalId(final int localId) {
        final String[] columns = { BtDeviceContract.BtDeviceEntry._ID };
        final String[] selectionArgs = { Integer.toString(localId) };

        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                BtDeviceContract.SQL_EQ_LOCAL_ID, selectionArgs, null, null, null, "1");

        int rowId = -1;
        // if BT_MAC found
        if ( cursor != null ) {

            if ( cursor.moveToFirst() ) {
                // return row _ID which is column index 0
                rowId = cursor.getInt(0);
            }

            // free resources
            cursor.close();
        }

        return rowId;
    }

    /**
     *
     * @param bt_mac  MAC address of BT device
     * @param localId  endpoint local id
     * @return  database row _ID or -1 on not found
     */
    public synchronized int setLocalId(final String bt_mac, final int localId) {

        ContentValues row = new ContentValues();
        row.put(BtDeviceContract.BtDeviceEntry.COLUMN_NAME_BT_LOCAL_ID, localId);

        String[] selectionArgs = { bt_mac };

        return mDb.update(BtDeviceContract.BtDeviceEntry.TABLE_NAME,
                row,
                BtDeviceContract.SQL_EQ_BT_MAC,
                selectionArgs);
    }

    /**
     *
     * @return number of BT devices
     */
    public int getNumRows() {
        final String[] columns = { BtDeviceContract.BtDeviceEntry._ID };
        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                null, null, null, null, null, null);

        int numRows = 0;
        if (cursor != null) {
            numRows = cursor.getCount();
            cursor.close();
        }
        return numRows;
    }

    /**
     * open the DB with write/read access or
     * just read access if that is all that is possible.
     *
     * @return this  BT device database adapter
     * @throws SQLException
     */
    public synchronized BtDeviceDb open() throws SQLException {
        Log.d(LOG_TAG, "open()");
        try {
            mDb = mDbHelper.getWritableDatabase();
        } catch (SQLException ex) {
            mDb = mDbHelper.getReadableDatabase();
        }
        return this;
    }

    /**
     * close the DB.
     */
    public synchronized void close() {
        Log.d(LOG_TAG, "close()");
        getDB().close();
    }

    /**
     * deletes BT Device database file
     */
    public synchronized boolean deleteDbFile() {
        boolean bSuccessful = false;

        if (mContext != null) {
            if ( mDbHelper != null ) {
                mDbHelper.close();
            }
            bSuccessful = mContext.deleteDatabase(BtDeviceDbHelper.DATABASE_NAME);
            if ( bSuccessful ) {
                Log.d(LOG_TAG, "Successfully deleted: " + BtDeviceDbHelper.DATABASE_NAME);
            }
            else {
                Log.e(LOG_TAG, "Failed to delete: " + BtDeviceDbHelper.DATABASE_NAME);
            }
        }
        return bSuccessful;
    }

    public static synchronized boolean deleteDbFile(Context context) {
        boolean bSuccessful = false;

        if (context != null) {
            bSuccessful = context.deleteDatabase(BtDeviceDbHelper.DATABASE_NAME);
            if ( bSuccessful ) {
                Log.d(LOG_TAG, "Successfully deleted: " + BtDeviceDbHelper.DATABASE_NAME);
            }
            else {
                Log.e(LOG_TAG, "Failed to delete: " + BtDeviceDbHelper.DATABASE_NAME);
            }
        }
        return bSuccessful;
    }


    /**
     * Get the underlying Database.
     *
     * @return
     */
    public SQLiteDatabase getDB() {
        return mDb;
    }
}
