package com.flex.agentdb;

import android.content.ContentValues;
import android.provider.BaseColumns;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Define package management database schema
 * <p>
 * Created by Alex.Molina@flex.com
 */

public final class PkgMgmtContract {
    // don't allow construction of new objects
    private PkgMgmtContract() {
    }

    /**
     * Define columns (fields) in row (record)
     * column names chosen to match JSON keys in Intel's List Packages and
     * Flex OTA manifest file
     */
    public static class PkgMgmtEntry implements BaseColumns {
        public static final String TABLE_NAME = "PKG_TABLE";
        public static final String PKG_INDEX = "PKG_INDEX";
        // Intel's ListPackages' keys
        public static final String COLUMN_NAME_PKG_NAME = "pkgName";
        public static final String COLUMN_NAME_PKG_VERSION = "pkgVersion";
        public static final String COLUMN_NAME_PKG_VERSION_CODE = "pkgVersionCode";
        public static final String COLUMN_NAME_PKG_RUNNING = "pkgRunning";
        public static final String COLUMN_NAME_SYSTEM_PKG = "systemPkg";
        // eApp-generated
        public static final String COLUMN_NAME_PKG_STATE = "pkgState";
    }

    public static final String[] ALL_COLUMN_NAMES = {
            PkgMgmtEntry.COLUMN_NAME_PKG_NAME,
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION,
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE,
            PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING,
            PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG,
            PkgMgmtEntry.COLUMN_NAME_PKG_STATE
    };

    public static final String[] LIST_PKGS_KEYS = {
            PkgMgmtEntry.COLUMN_NAME_PKG_NAME,
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION,
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE,
            PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING,
            PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG
    };

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PkgMgmtEntry.TABLE_NAME +
                    " (" +
                    PkgMgmtEntry._ID + " INTEGER PRIMARY KEY," +
                    PkgMgmtEntry.COLUMN_NAME_PKG_NAME + " TEXT NOT NULL," +
                    PkgMgmtEntry.COLUMN_NAME_PKG_VERSION + " TEXT," +
                    PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE + " TEXT," +
                    PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING + " BOOLEAN," +
                    PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG + " BOOLEAN," +
                    PkgMgmtEntry.COLUMN_NAME_PKG_STATE + " INTEGER," +
                    "CONSTRAINT PKG_UNIQUE UNIQUE ( " +
                    PkgMgmtEntry.COLUMN_NAME_PKG_NAME + " ) " +
                    ")";

    public static final String SQL_CREATE_PKG_INDEX =
            "CREATE UNIQUE INDEX IF NOT EXISTS " + PkgMgmtEntry.PKG_INDEX +
                    " ON " + PkgMgmtEntry.TABLE_NAME + " ( " +
                    PkgMgmtEntry.COLUMN_NAME_PKG_NAME + " ASC )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PkgMgmtEntry.TABLE_NAME;

    public static final String SQL_ADD_ENTRY =
            "INSERT INTO " + PkgMgmtEntry.TABLE_NAME;

    public static final String SQL_EQ_PKG_NAME =
            PkgMgmtEntry.COLUMN_NAME_PKG_NAME + " = ?";

    public static final String SQL_EQ_PKG_VERSION =
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION + " = ?";

    public static final String SQL_EQ_PKG_VERSION_CODE =
            PkgMgmtEntry.COLUMN_NAME_PKG_VERSION_CODE + " = ?";

    public static ContentValues initRow() {
        ContentValues row = new ContentValues();

        for (String key : ALL_COLUMN_NAMES) {
            if (key.equals(PkgMgmtEntry.COLUMN_NAME_PKG_RUNNING)
                    || key.equals(PkgMgmtEntry.COLUMN_NAME_SYSTEM_PKG)) {
                // boolean type
                row.put(key, false);
            } else if (key.equals(PkgMgmtEntry.COLUMN_NAME_PKG_STATE)) {
                // integer type
                row.put(key, 0);
            } else {
                // text type
                row.put(key, "");
            }
        }

        return row;
    }

}
