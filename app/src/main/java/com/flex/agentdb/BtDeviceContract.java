package com.flex.agentdb;

import android.provider.BaseColumns;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * define BT Device database schema
 *
 * Created by Alex.Molina@flex.com on 7/7/17.
 */

public final class BtDeviceContract {
    private BtDeviceContract() {}

    public static class BtDeviceEntry implements BaseColumns {
        public static final String TABLE_NAME = "BT_DEVICE";
        public static final String COLUMN_NAME_BT_MAC = "BT_MAC";
        public static final String COLUMN_NAME_BT_NAME = "BT_NAME";
        public static final String COLUMN_NAME_BT_LOCAL_ID = "BT_LOCAL_ID";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BtDeviceEntry.TABLE_NAME +
                    " (" +
                    BtDeviceEntry._ID + " INTEGER PRIMARY KEY," +
                    BtDeviceEntry.COLUMN_NAME_BT_MAC + " TEXT," +
                    BtDeviceEntry.COLUMN_NAME_BT_NAME + " TEXT," +
                    BtDeviceEntry.COLUMN_NAME_BT_LOCAL_ID + " INTEGER" +
                    ")";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BtDeviceEntry.TABLE_NAME;

    public static final String SQL_ADD_ENTRY =
            "INSERT INTO " + BtDeviceEntry.TABLE_NAME ;

    public static final String SQL_EQ_BT_MAC =
            BtDeviceEntry.COLUMN_NAME_BT_MAC + " = ?";

    public static final String SQL_EQ_LOCAL_ID =
            BtDeviceEntry.COLUMN_NAME_BT_LOCAL_ID + " = ?";

    public static final String SQL_CONTAINS_NAME =
            BtDeviceEntry.COLUMN_NAME_BT_NAME + " LIKE ?";
}
