package com.flex.agentdb;

import android.content.ContentValues;
import android.provider.BaseColumns;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Manifest image files database schema
 * <p>
 * Created by Alex.Molina@flex.com
 */

public final class ImageMgmtContract {
    private ImageMgmtContract() {
    }

    /**
     * Define columns (fields) in row (record)
     * column names chosen to match JSON keys in Flex OTA manifest file
     */
    public static class ImageMgmtEntry implements BaseColumns {
        public static final String TABLE_NAME = "IMAGE_TABLE";
        public static final String IMAGE_INDEX = "IMAGE_INDEX";
        // Flex OTA manifest file
        public static final String COLUMN_NAME_IMAGE_TYPE = "type";
        public static final String COLUMN_NAME_IMAGE_VERSION = "version";
        public static final String COLUMN_NAME_IMAGE_NAME = "name";
        public static final String COLUMN_NAME_IMAGE_HASH = "imageHash";
        public static final String COLUMN_NAME_IMAGE_URL = "imageUrl";
        // eApp-generated
        public static final String COLUMN_NAME_IMAGE_PATH = "imagePath";
        public static final String COLUMN_NAME_IMAGE_STATE = "imageState";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ImageMgmtEntry.TABLE_NAME +
                    " (" +
                    ImageMgmtEntry._ID + " INTEGER PRIMARY KEY," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_TYPE + " TEXT NOT NULL, " +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_VERSION + " TEXT NOT NULL," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME + " TEXT NOT NULL," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_HASH + " TEXT NOT NULL," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_URL + " TEXT NOT NULL," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_PATH + " TEXT NOT NULL," +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_STATE + " INTEGER, " +
                    "CONSTRAINT NAME_UNIQUE UNIQUE (" +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME + ")" +
                    ")";

    /*
     * All columns in Image files database
     */
    public static final String[] ALL_COLUMN_NAMES = {
            ImageMgmtEntry.COLUMN_NAME_IMAGE_TYPE,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_VERSION,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_HASH,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_URL,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_PATH,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_STATE
    };

    /*
     *  All keys in Flex manifest for an image package
     */
    public static final String[] MANIFEST_PKG_KEYS = {
            ImageMgmtEntry.COLUMN_NAME_IMAGE_TYPE,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_VERSION,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_HASH,
            ImageMgmtEntry.COLUMN_NAME_IMAGE_URL
    };

    public static final String SQL_CREATE_IMAGE_INDEX =
            "CREATE UNIQUE INDEX IF NOT EXISTS " + ImageMgmtEntry.IMAGE_INDEX +
                    " ON " + ImageMgmtEntry.TABLE_NAME + " ( " +
                    ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME + " ASC )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ImageMgmtEntry.TABLE_NAME;

    public static final String SQL_ADD_ENTRY =
            "INSERT INTO " + ImageMgmtEntry.TABLE_NAME;

    public static final String SQL_EQ_IMAGE_NAME =
            ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME + " = ?";

    public static final String SQL_EQ_IMAGE_VERSION =
            ImageMgmtEntry.COLUMN_NAME_IMAGE_VERSION + " = ?";

    public static final String SQL_CONTAINS_APK =
            ImageMgmtEntry.COLUMN_NAME_IMAGE_URL + " LIKE ?";

    /*
     * Since both SQLite and JSON don't understand Java enums
     * just declare simple ints for now, EnumMap seemed overkill
     */
    public static final int NOT_DL = 0;  // not in download cache and not installed
    public static final int DL_ONLY = 1;  // downloaded, but not installed
    public static final int INSTALLING = 2;  // reqInstallAPKs started, not yet completed
    public static final int INSTALLED = 3;  // APK fully installed
    public static final int INSTALL_FAILED = 4;  // APK failed to install
    public static final int UNINSTALLING = 5;  // reqUninstallAPKs started, not yet completed
    public static final int UNINSTALLED = 6;  // uninstall sucessful, APK file may still be in cache
    public static final int UNINSTALL_FAILED = 7;  //  uninstall failed


    public static ContentValues initRow() {
        ContentValues row = new ContentValues();

        /* columns from manifest packages entry */
        for (String key : MANIFEST_PKG_KEYS) {
            // text type
            row.put(key, "");
        }

            /* eApp-generated data */
        row.put(ImageMgmtEntry.COLUMN_NAME_IMAGE_PATH, "");
        row.put(ImageMgmtEntry.COLUMN_NAME_IMAGE_STATE, NOT_DL);
        return row;
    }

}
