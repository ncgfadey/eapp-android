package com.flex.agentdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import static com.flex.agentdb.ImageMgmtContract.ALL_COLUMN_NAMES;
import static com.flex.agentdb.ImageMgmtContract.ImageMgmtEntry;
import static com.flex.agentdb.ImageMgmtContract.MANIFEST_PKG_KEYS;
import static com.flex.agentdb.ImageMgmtContract.SQL_CONTAINS_APK;
import static com.flex.agentdb.ImageMgmtContract.SQL_EQ_IMAGE_NAME;
import static com.flex.agentdb.ImageMgmtContract.initRow;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Manifest image files database
 * <p>
 * Created by Alex.Molina@flex.com
 */

public class ImageMgmtDb {
    private SQLiteDatabase mDb = null;

    public static final String LOG_TAG = "AGENT_IMAGE_DB";

    ImageMgmtDb(Context context) {
        // throws NullPointerException if fails
        mDb = PkgMgmtDbHelper.getPkgMgmtDbHelper(context).open();
    }

    /**
     * Inserts new Image entry
     *
     * @param manifestPkgJobj JSON object of manifest package entry
     * @return ImageMgmt database row _ID
     */

    public long insert(final JSONObject manifestPkgJobj) {
        long rowId = -1;

        try {
            if (manifestPkgJobj == null) {
                throw new NullPointerException("manifestPkgJobj");
            }

            String imageName = getImageName(manifestPkgJobj);

            if ((imageName != null) && !imageName.isEmpty()) {
                rowId = findByImageName(imageName);

                // insert if imageName not in database
                if (rowId == -1) {
                    Log.d(LOG_TAG, "insert new imageName: " + imageName);

                    ContentValues row = initRow();
                    setRowFromManifestPkg(manifestPkgJobj, row);
                    rowId = mDb.insert(ImageMgmtEntry.TABLE_NAME, null, row);
                } else {
                    Log.d(LOG_TAG, "DB already has imageName: " + imageName);
                }
            }
        } catch (Exception error) {
            Log.e(LOG_TAG, "insert failed: " + error.toString());
        }

        return rowId;
    }

    /**
     * @param manifestJobj JSON object of a single manifest package entry, may be null
     * @return database row _ID or -1 on not found
     */
    public int update(final JSONObject manifestJobj) {

        int rowId = -1;
        String imageName = null;

        try {
            ContentValues row = initRow();
            imageName = getImageName(manifestJobj);

            if ((imageName != null) && !imageName.isEmpty()) {

                row.put(ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME, imageName);

                if (manifestJobj != null) {
                    setRowFromManifestPkg(manifestJobj, row);
                }

                String[] selectionArgs = {imageName};

                rowId = mDb.update(ImageMgmtEntry.TABLE_NAME,
                        row,
                        SQL_EQ_IMAGE_NAME,
                        selectionArgs);
            }
        } catch (Exception err) {
            Log.e(LOG_TAG, "update failed: " + err.toString());
        }

        return rowId;
    }

    /**
     * Update imageState for the the given imageName
     *
     * @param imageName  image package to update
     * @param imageState new imageState value
     * @return
     */
    public int updateImageState(String imageName, int imageState) {
        int rowId = -1;

        try {
            ContentValues row = new ContentValues();

            if ((imageName != null) && !imageName.isEmpty()) {

                row.put(ImageMgmtEntry.COLUMN_NAME_IMAGE_STATE, imageState);

                String[] selectionArgs = {imageName};

                rowId = mDb.update(ImageMgmtEntry.TABLE_NAME,
                        row,
                        SQL_EQ_IMAGE_NAME,
                        selectionArgs);
            }
        } catch (Exception err) {
            Log.e(LOG_TAG, "updateImageState failed: " + err.toString());
        }

        return rowId;

    }

    /**
     * Gets imageName (key="name") in manifest JSON object
     *
     * @param manifestPkgJobj
     * @return String image name
     * @throws JSONException
     */
    public String getImageName(JSONObject manifestPkgJobj)
            throws JSONException {
        String imageName = null;

        if (manifestPkgJobj != null) {
            imageName = manifestPkgJobj.getString(ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME);
        }
        return imageName;
    }

    /**
     * set row's column values for Flex OTA manifest
     *
     * @param manifestJobj JSON of single manifest package entry
     * @param row          row column values to update
     * @throws JSONException
     */
    public void setRowFromManifestPkg(JSONObject manifestJobj, ContentValues row)
            throws JSONException {
        try {
            for (String key : MANIFEST_PKG_KEYS) {
                row.put(key, manifestJobj.getString(key));
            }
        } catch (JSONException jsonError) {
            Log.e(LOG_TAG, jsonError.toString());
            throw jsonError;
        }
    }

    /**
     * Deletes image entry given imageName
     *
     * @param imageName manifest image name
     * @return number of package management entries deleted or -1 if not found/error
     */
    public int deleteByImage(final String imageName) {
        String[] selectionArgs = {imageName};

        return mDb.delete(ImageMgmtEntry.TABLE_NAME,
                SQL_EQ_IMAGE_NAME,
                selectionArgs);
    }

    /**
     * Deletes image entry by APK name
     *
     * @param apkName APK filename to search for
     * @return number of rows deleted or -1 for error
     */
    public int deleteByApk(final String apkName) {
        String[] selectionArgs = {apkName};

        return mDb.delete(ImageMgmtEntry.TABLE_NAME,
                SQL_CONTAINS_APK,
                selectionArgs);

    }

    /**
     * Get image_url given APK name (contained in image url)
     *
     * @param apkName
     * @return String manifest image name = Android package name
     */
    public String getPkgForApk(final String apkName) {
        final String[] columns = {ImageMgmtEntry.COLUMN_NAME_IMAGE_NAME};
        final String[] selectionArgs = {apkName};

        Cursor cursor = mDb.query(BtDeviceContract.BtDeviceEntry.TABLE_NAME, columns,
                SQL_CONTAINS_APK, selectionArgs, null, null, null, "1");

        String pkgName = null;
        // if Android package found
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                // return pkgName in column 0 of result set
                pkgName = cursor.getString(0);
            }

            // free resources
            cursor.close();
        }

        return pkgName;
    }


    /**
     * lookup by imageName for image_url (contains APK filename)
     *
     * @param imageName Android package name == Manifest Image name
     * @return String imageUrl
     */
    public String getImageUrlByImageName(final String imageName) {
        final String[] columns = {ImageMgmtEntry.COLUMN_NAME_IMAGE_URL};
        final String[] selectionArgs = {imageName};

        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME, columns,
                SQL_EQ_IMAGE_NAME, selectionArgs, null, null, null, "1");

        String imageUrl = null;
        // if  Android package name found
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                // return image_url in column 0 of result set
                imageUrl = cursor.getString(0);
            }

            // free resources
            cursor.close();
        }

        return imageUrl;
    }

    /**
     * returns entire row for imageName in image files database
     * this includes the imageState
     *
     * @param imageName
     * @return
     * @throws JSONException
     */
    public JSONObject getRowByImageName(final String imageName)
            throws JSONException {
        final String[] selectionArgs = {imageName};

        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME,
                ALL_COLUMN_NAMES,
                SQL_EQ_IMAGE_NAME, selectionArgs, null, null, null, "1");

        JSONObject jobj = new JSONObject();
        // if  Android package name found
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    // set entire row
                    copyColsJson(cursor, jobj);
                }
            } finally {
                // free resources
                cursor.close();
            }
        }

        return jobj;
    }


    /**
     * @param apkName APK filename in image_url
     * @return JSON object of row
     */
    public JSONObject getRowByApk(final String apkName)
            throws JSONException {

        final String[] selectionArgs = {apkName};

        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME,
                ALL_COLUMN_NAMES,
                SQL_CONTAINS_APK, selectionArgs, null, null, null, "1");

        JSONObject row = new JSONObject();

        // if APK filename found
        if (cursor != null) {
            try {

                if (cursor.moveToFirst()) {
                    // return entire row
                    copyColsJson(cursor, row);
                }
            } finally {
                // free resources
                cursor.close();
            }
        }

        return row;
    }

    /**
     * Copy column values from SQL row to JSON object
     * Copies String(Text) and integers, other types replaced by empty string
     *
     * @param cursor SQL row to copy values from
     * @param jobj   JSON object to copy values into
     * @throws JSONException
     */
    private void copyColsJson(Cursor cursor, JSONObject jobj)
            throws JSONException {
        int i = 0;
        try {
            // set entire row
            for (i = 0; i < cursor.getColumnCount(); i++) {
                int field_type = cursor.getType(i);

                if (field_type == Cursor.FIELD_TYPE_STRING) {
                    jobj.put(ALL_COLUMN_NAMES[i], cursor.getString(i));
                } else if (field_type == Cursor.FIELD_TYPE_INTEGER) {
                    jobj.put(ALL_COLUMN_NAMES[i], cursor.getInt(i));
                } else {
                    jobj.put(ALL_COLUMN_NAMES[i], "");
                }
            }
        } catch (JSONException jsonErr) {
            Log.e(LOG_TAG, "JSON failure on " + ALL_COLUMN_NAMES[i]);
            throw jsonErr;
        }
    }

    /**
     * @param imageName Manifest image package name
     * @return ImageMgmtEntry database row _ID or -1 if not found
     */
    public int findByImageName(final String imageName) {
        final String[] columns = {ImageMgmtEntry._ID};
        final String[] selectionArgs = {imageName};

        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME, columns,
                SQL_EQ_IMAGE_NAME, selectionArgs, null, null, null, "1");

        int rowId = -1;
        // if imageName found
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                // return row _ID which is column index 0
                rowId = cursor.getInt(0);
            }
            // free resources
            cursor.close();
        }
        return rowId;
    }


    /**
     * @param ApkName APK name in Manifest imageUrl
     * @return ImageMgmtEntry database row _ID or -1 if not found
     */
    public int findByApkName(final String ApkName) {
        final String[] columns = {ImageMgmtEntry._ID};
        final String[] selectionArgs = {ApkName};

        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME, columns,
                SQL_CONTAINS_APK, selectionArgs, null, null, null, "1");

        int rowId = -1;
        // if APK name found
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    // return row _ID which is column index 0
                    rowId = cursor.getInt(0);
                }
            } finally {
                // free resources
                cursor.close();
            }
        }
        return rowId;
    }

    /**
     * @return number of Image entries
     */
    public int getNumRows() {
        final String[] columns = {ImageMgmtEntry._ID};
        Cursor cursor = mDb.query(ImageMgmtEntry.TABLE_NAME, columns,
                null, null, null, null, null, null);

        int numRows = 0;
        if (cursor != null) {
            numRows = cursor.getCount();
            cursor.close();
        }
        return numRows;
    }
}
