/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

package com.flex.eapp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class EAppExceptionHandler implements Thread.UncaughtExceptionHandler {
    public static final String TAG = "EAppExceptionHandler";
    private final int RESTART_DELAY_MS = 5000;


    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.d(TAG, "Exception in thread " + t.toString());
        e.printStackTrace();

        /* On exception, we cannot be completely sure of state.
         * Restrict cleanup to static calls.
         */
        EAppJob.cancelAllJobs();

        Context context = EApp.getAppContext();
        Intent intent = new Intent(context, EAppActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EAppActivity.EXTRA_START_REASON, EAppActivity.START_REASON_EXCEPTION);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, intent, PendingIntent.FLAG_ONE_SHOT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + RESTART_DELAY_MS,
                pendingIntent);
        System.exit(2);
    }
}
