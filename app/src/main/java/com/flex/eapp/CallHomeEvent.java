package com.flex.eapp;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Alex.Molina@flex.com
 */
class CallHomeEvent extends RmtMgmtEvent {
    CallHomeEvent(JSONObject eventObj)
            throws JSONException {
        super(eventObj);
        Log.d(RmtMgmtEvent.TAG, EventTypes.CALL_HOME.toString());

    }
}
