package com.flex.eapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.intel.rmtmgmtprov.api.Common;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */
/* Background service responding to events from RmtMgmtProv and
 * agent and queues them for EAppController to process.
 */
public class EAppService extends Service {
    private static final String TAG = EAppService.class.getSimpleName();

    static enum STATUS_CODE {
        SUCCESS(0),
        JSON_EXCEPTION(1),
        API_REMOTE_EXCEPTION(2),
        API_RESPONSE_ERROR(3),
        BIND_FAILED(4),
        INVALID_PARAMETER(5),
        TIMEOUT(6),
        NOT_READY(7),
        AGENT_ERROR(8);

        private int value;
        static final String[] StatusCodeStrings = new String[] {
                "Success",
                "A JSON exception occured while parsing request",
                "A remote exception occured while sending API request",
                "Response to API request failed",
                "Bind to EAppService failed",
                "Invalid parameter",
                "Timeout",
                "Not ready",
                "Agent error - check logs"
        };

        private STATUS_CODE(int value) {
            this.value = value;
        }

        public String toString() {
            return StatusCodeStrings[this.value];
        }
    }

    /* Requests that can be made to service using intents, CI_MSG
     * and CI_EVENT are used by Agent object. A helper function for
     * RmtMgmt object is implemented within EAppService
     */
    static final String REQ_START_SERVICE_FROM_ACTIVITY = BuildConfig.APPLICATION_ID + ".StartServiceActivity";
    static final String REQ_CI_MSG = BuildConfig.APPLICATION_ID + ".CIMsg";
    static final String REQ_CI_EVENT = BuildConfig.APPLICATION_ID + ".CIEvent";
    static final String REQ_RMT_MGMT_EVENT = BuildConfig.APPLICATION_ID + ".RmtMgmtEvent";

    static final String PARAM_RMT_MGMT_EVENT_DATA = "eventData";

    private RmtMgmt mRmtMgmt;
    private EAppController mEAppController;
    private boolean filesCopied;

    HandlerThread thread;
    private volatile Looper mServiceLooper;
    private volatile EAppController.EAppControllerHandler mEAppControllerHandler;

    static void processRmtMgmtEvent(JSONObject event) {
        Context context = EApp.getAppContext();
        String msg = event.toString();
        Intent intent = new Intent(context, EAppService.class);
        intent.setAction(REQ_RMT_MGMT_EVENT);
        intent.putExtra(PARAM_RMT_MGMT_EVENT_DATA, msg);
        context.startService(intent);
    }


    public class EAppServiceBinder extends Binder {
        public EAppService getService() {
            return EAppService.this; /* Give bound object access to service */
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent notificationIntent = new Intent(this, EAppActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification n = new Notification();
        startForeground(1, n);

        mRmtMgmt = new RmtMgmt();
        mEAppController = new EAppController();

        /* Create a thread on which service requests will be run.
         * Thread will be used to continue possibly long-running init
         * including starting and restarting rmtmgmt and controller.
         */
        thread = new HandlerThread("EAppServiceThread");
        thread.start();
        mServiceLooper = thread.getLooper();

        /* Once handler is attached to looper, we bind controller
         * to thread so that it can start sending requests. On connection,
         * controller will send its first request that starts services
         */
        mEAppControllerHandler = mEAppController.new EAppControllerHandler(mServiceLooper);
        STATUS_CODE status = mEAppController.bindToService();
        if (status != STATUS_CODE.SUCCESS) {
            Log.e(TAG, "Unable to bind controller");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent == null) {
            Log.e(TAG, "Null intent sent (restart?)");
            return START_STICKY;
        }

        String action = intent.getAction();
        //Log.d(TAG, "Received intent:" + action + " id:" + startId);
        if ((flags & START_FLAG_REDELIVERY) != 0) {
            Log.i(TAG, "Redelivery of intent:" + action);
        }
        if ((flags & START_FLAG_RETRY) != 0) {
            Log.i(TAG, "Retry of intent:" + action);
        }

        if (REQ_START_SERVICE_FROM_ACTIVITY.equals(action)) {
            Log.d(TAG, "Service started from activity");
            if (!filesCopied) {
                setupDataFiles();
                filesCopied = true;
            }
            return START_STICKY;
        }
        else if (REQ_CI_MSG.equals(action)) {
            if (!mEAppController.isReady()) {
                stopSelf(startId);
                Log.e(TAG, "Controller not ready for CI message");
                return START_NOT_STICKY;
            }
            String strMsg = intent.getStringExtra("msg");
            Message msg = mEAppControllerHandler.obtainMessage(
                    EAppController.REQ_CI_MSG,
                    startId,
                    0,
                    strMsg);
            mEAppControllerHandler.sendMessage(msg);
            return START_NOT_STICKY;
        }
        else if (REQ_CI_EVENT.equals(action)) {
            if (!mEAppController.isReady()) {
                stopSelf(startId);
                Log.e(TAG, "Controller not ready for CI event");
                return START_NOT_STICKY;
            }
            int event = intent.getIntExtra("event", -1);
            String strMsg = intent.getStringExtra("msg");
            Message msg = mEAppControllerHandler.obtainMessage(
                    EAppController.REQ_CI_EVENT,
                    startId,
                    event,
                    strMsg);
            mEAppControllerHandler.sendMessage(msg);
            return START_NOT_STICKY;
        }
        else if (REQ_RMT_MGMT_EVENT.equals(action)) {
            /* Event is coming from RmtMgmt - it has no knowledge of
             * controller state. If controller is down, do not pass
             * to handler.
             */
            if (!mEAppController.isReady()) {
                stopSelf(startId);
                Log.e(TAG, "Controller not ready for RmtMgmt event");
                return START_NOT_STICKY;
            }
            String strMsg = intent.getStringExtra(PARAM_RMT_MGMT_EVENT_DATA);
            Message msg = mEAppControllerHandler.obtainMessage(
                    EAppController.RMT_MGMT_EVENT,
                    startId,
                    0,
                    strMsg);
            mEAppControllerHandler.sendMessage(msg);
            return START_NOT_STICKY;
        }
        else {
            Log.e(TAG, "Unhandled action: " + action);
            /* We should stop startI and return START_NOT_STICKY here
             * but we return START_STICKY as default for future messages.
             */
            stopSelf(startId);
            return START_NOT_STICKY;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new EAppServiceBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        boolean stopService = false;
        Bundle b = intent.getExtras();
        if (b != null) {
            stopService = b.getBoolean("STOP_SERVICE");
        }
        if (stopService) {
            stopSelf();
        }

        return false;
    }

    @Override
    public void onTrimMemory(int level) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_COMPLETE) {
            Log.e(TAG, "Eapp about to be killed due to low memeory");
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "Stopping service");

        /* The first message sent, due to connection created at the end
         * of onCreate, is to start services. Stop them first. Controller
         * will have already unbound itself and called stopSelf om service.
         */
        mEAppController.stop();
        mRmtMgmt.stop();

        mServiceLooper.quitSafely();

        super.onDestroy();
        System.exit(0);
    }

    /* Functions to access RmtMgmt on behalf of controller */
    JSONObject RmtMgmt_ListPackages()
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_LIST_PACKAGES);

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_StartPackages(String[] pkgs)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_START_PACKAGES);
        status = req.putParam(Common.PARAM_PKG_NAMES, pkgs);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_StopPackages(String[] pkgs)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_STOP_PACKAGES);
        status = req.putParam(Common.PARAM_PKG_NAMES, pkgs);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_GetDateTime()
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_GET_DATE_TIME);
        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_SetDateTime(String dateTime, String timeZone)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_SET_DATE_TIME);
        status = req.putParam(Common.PARAM_AUTO_TIME, 0);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        status = req.putParam(Common.PARAM_AUTO_TIME_ZONE, 0);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        if (dateTime != null) {
            status = req.putParam(Common.PARAM_DATE_TIME, dateTime);
            if (status != STATUS_CODE.SUCCESS) {
                return null;
            }
        }
        if (timeZone != null) {
            status = req.putParam(Common.PARAM_TIME_ZONE, timeZone);
            if (status != STATUS_CODE.SUCCESS) {
                return null;
            }
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_GetDeviceInfo(String filter)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_GET_DEVICE_INFO);
        status = req.putParam(Common.PARAM_FILTER, filter);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_InstallAPKs(String[] apks, boolean reboot)
    {
        return RmtMgmt_InstallAPKs(apks, reboot, 0);
    }

    JSONObject RmtMgmt_InstallAPKs(String[] apks, boolean reboot, long timeoutMs)
    {
        EAppService.STATUS_CODE status;
        RmtMgmt.Request req;

        if (timeoutMs == 0) {
            req = mRmtMgmt.new Request(Common.REQUEST_INSTALL_APKS);
        }
        else
        {
            req = mRmtMgmt.new Request(Common.REQUEST_INSTALL_APKS, timeoutMs);
        }

        status = req.putParam(Common.PARAM_FILE_PATHS, apks);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        status = req.putParam(Common.PARAM_REBOOT, reboot);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_UninstallAPKs(String[] pkgs)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_UNINSTALL_APKS);
        status = req.putParam(Common.PARAM_PKG_NAMES, pkgs);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_Reboot()
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_REBOOT);
        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    JSONObject RmtMgmt_Provision(String path)
    {
        EAppService.STATUS_CODE status;

        RmtMgmt.Request req = mRmtMgmt.new Request(Common.REQUEST_PROVISIONING);
        status = req.putParam(Common.PARAM_FILE_PATH, path);
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }

        status = req.send();
        if (status != STATUS_CODE.SUCCESS) {
            return null;
        }
        return req.getResponse();
    }

    RmtMgmt getRmtMgmt() {
        return mRmtMgmt;
    }

    EAppController getController() {
        return mEAppController;
    }

    EAppController.EAppControllerHandler getHandler() {
        return mEAppControllerHandler;
    }

    void setupDataFiles()
    {
        final String certFilename = "ca-bundle.crt";
        final String certDestPath = getFilesDir().getPath() + "/" + certFilename;

        if ( ! copyAssetFile(certFilename, certDestPath) ) {
            Log.e(TAG, "Failed to setup " + certDestPath);
        }

        final String agentDbFilename = "agent.db";
        final String agentDbDestPath = getExternalFilesDir(null).getPath() + "/" + agentDbFilename;

        if ( ! copyAssetFile(agentDbFilename, agentDbDestPath) ) {
            Log.e(TAG, "Failed to setup " + agentDbDestPath);
        }
    }

    boolean copyAssetFile(final String srcFilename, final String destPath) {
        boolean was_successful = true;

        if ((new File(destPath)).length() > 0) {
            Log.d(TAG, destPath + " already exists and non-empty");
            return true;
        }

        try (InputStream assetFile = getAssets().open(srcFilename, AssetManager.ACCESS_STREAMING)) {
            try (FileOutputStream destFile = new FileOutputStream(destPath)) {
                final int MAX_BUF = 64 * 1024;
                byte[] buffer = new byte[MAX_BUF];

                while ( assetFile.available() > 0 ) {
                    int nbytes_read = assetFile.read(buffer);
                    if (nbytes_read > 0 ) {
                        destFile.write(buffer, 0, nbytes_read);
                    }
                }  // end while

            } catch (IOException e) {
                was_successful = false;
                e.printStackTrace();
            }
        } catch (IOException e) {
            was_successful = false;
            e.printStackTrace();
        }

        return was_successful;
    }
}
