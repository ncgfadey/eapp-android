package com.flex.eapp;

import org.json.JSONException;
import org.json.JSONObject;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Alex.Molina@flex.com
 */

class BtEvent extends RmtMgmtEvent {
    public static final String BT_MAC_KEY = "BTMacAddr";
    public static final String BT_NAME_KEY = "BTName";

    protected final String mBtMacAddr;
    protected final String mBtName;

    public BtEvent(JSONObject eventObj) throws JSONException {
        super(eventObj);

        mBtMacAddr = eventObj.getString(BT_MAC_KEY);
        mBtName = eventObj.getString(BT_NAME_KEY);
    }

    public String getmBtMacAddr() {
        return mBtMacAddr;
    }

    public String getmBtName() {
        return mBtName;
    }
}
