package com.flex.eapp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.File;



/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */

/* EAppAgent - encapsulate agent library -specific logic */
class EAppAgent {
    private final static String TAG = EAppAgent.class.getSimpleName();

    private EAppController mController;
    private static boolean mReady = false;

    private String filesDir;
    private String extFilesDir;

    EAppAgent(EAppController controller) {
        mController = controller;
        Context context = EApp.getAppContext();
        filesDir =  context.getFilesDir().getPath();
        extFilesDir = context.getExternalFilesDir(null).getPath();
    }

    static enum FLX_CI_RESULT {
        OK(0),
        ERROR(1),
        TIMEOUT(2),
        NOT_ACTIVATED(3),
        INVALID_PARAM(4),
        MAX_CONNECTION_LIMIT(5),
        ALREADY_ACTIVATED(6),
        OUT_OF_MEMORY(7),
        PARSE_ERROR(8),
        ALREADY_CONNECTED(9),
        PARSE_ERROR_URI(10),
        PAL_ERROR(11),
        MQTT_CONNECT_ERROR(12),
        MQTT_SUBSCRIBE_ERROR(13),
        STRING_TOO_LONG(14),
        MQTT_PUBLISH_ERROR(15),
        INVALID_REQUEST(16),
        NVM_FAIL(17),
        ATTRIBUTE_NOT_FOUND(18),
        STATIC_TOO_SMALL(19),
        TYPE_MISMATCH(20),
        NETWORK_ERROR(21),
        ACTIVATION_DATA_MISSING(22),
        OTA_CHECK_FAILED(23),
        JSON_OUT_OF_TOKENS(24),
        JSON_PARSE_ERROR(25),
        ATTRIBUTES_FAIL(26),
        NO_ATTRIBUTES(27),
        BUSY(28),
        ALREADY_DISCONNECTED(29),
        ENDPOINT_NOT_FOUND(30),
        NOT_INIT(31),
        CB_FAIL(32),
        DATABASE_ERROR(33),
        SETTING_NOT_FOUND(34),
        ENDPOINT_ALREADY_EXISTS(35),
        OTA_REQUIRED(36),
        NEW_EP_ID_ERROR(37),
        DTAT_TYPE_NOT_FOUND(38),
        UNKNOWN_RESULT(-1);

        private int value;
        static final String[] StatusCodeStrings = new String[] {
                "Success",
                "Generic error",
                "Operation timed out",
                "Device needs to be activated",
                "Invalid parameter supplied",
                "Max number of connections exceeded",
                "Device is already activated",
                "Out of memory",
                "Generic Parse error ",
                "MQTT already connected",
                "Error parsing URI",
                "Error in the platform layer",
                "Error establishing the MQTT connection",
                "MQTT subscribe error",
                "String too long",
                "MQTT publish error",
                "Invalid request",
                "Error reading or writing NVM",
                "Attribute not found",
                "Bufer too small",
                "Type mismatch",
                "Network error",
                " Missing activation data. (NVM could be missing)",
                "OTA check failed. (Could be invalid manifest or manifest signature)",
                "Out of memory for parsing JSON",
                "Error parsing JSON",
                "Activate/sync succeeded, but endpoint/attribute request failed. Sync to try again",
                "No attributes available",
                "Agent busy, try again",
                "MQTT already disconnected",
                "Endpoint local Id not found",
                "Agent not initialized. Call init",
                "Failed to call callback",
                "Error access database file",
                "Setting not found. (Invalid setting ID)",
                "Endpoint local Id already exist",
                "OTA required. The device must update before it can activate or sync",
                "Failed to generate new endpoint Id entry",
                "Failed to Invalid Data Type",
                "Unknown result"
        };

        private FLX_CI_RESULT(int value) {
            this.value = value;
        }

        private int toValue() {
            return value;
        }

        private static FLX_CI_RESULT[] mValues = FLX_CI_RESULT.values();

        public boolean CompareTo(int i){
            return value == i;
        }

        public static FLX_CI_RESULT toResult(int r) {
            for (int i = 0; i < mValues.length; i++) {
                if (mValues[i].CompareTo(r)) {
                    return mValues[i];
                }
            }
            return UNKNOWN_RESULT;
        }

        public String toString() {
            return StatusCodeStrings[this.value];
        }
    }

    public static final int CI_EVENT_NEW_OTA_IMAGE_AVAIL    = 0;
    public static final int CI_EVENT_IOTHUB_CONNECTION_DOWN = 1;
    public static final int CI_EVENT_IOTHUB_CONNECTION_UP   = 2;
    public static final int CI_EVENT_WIFI_CONNECTION_DOWN   = 3;
    public static final int CI_EVENT_WIFI_CONNECTION_UP     = 4;
    public static final int CI_EVENT_TIMESTAMP_AVAILABLE    = 5;
    public static final int CI_EVENT_EVT_MQTT_SEND_SUCCESS  = 6;
    public static final int CI_EVENT_INVALID_OTA_IMAGE      = 7;

    FLX_CI_RESULT init() {
        int r = jniInit();
        return FLX_CI_RESULT.toResult(r);
    }

    String getAgentLibVersion() {
        return jniGetAgentLibVersion();
    }

    FLX_CI_RESULT getActivationStatus() {
        int r = jniGetActivationStatus();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT activate() {
        int r = jniActivate();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sync() {
        int r = jniSync();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT connect() {
        int r = jniConnect();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT disconnect() {
        int r = jniDisconnect();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT clearActivationData() {
        int r = jniClearActivationData();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT deactivate() {
        int r = jniDeactivate();
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sendStringAttributeMsg(int epId, int localId, String value) {
        int r = jniSendStringAttributeMsg(epId, localId, value);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sendIntAttributeMsg(int epId, int localId, int value) {
        int r = jniSendIntAttributeMsg(epId, localId, value);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sendLongAttributeMsg(int epId, int localId, long value) {
        int r = jniSendLongAttributeMsg(epId, localId, value);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sendFloatAttributeMsg(int epId, int localId, float value) {
        int r = jniSendFloatAttributeMsg(epId, localId, value);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT sendBooleanAttributeMsg(int epId, int localId, boolean value) {
        int r = jniSendBooleanAttributeMsg(epId, localId, value);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT addDynamicEndpoint(int epId, String mfgId, String userField, String name) {
        int r = jniAddDynamicEndpoint(epId, mfgId, userField, name);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT deleteDynamicEndpoint(int epId) {
        int r = jniDeleteDynamicEndpoint(epId);
        return FLX_CI_RESULT.toResult(r);
    }

    FLX_CI_RESULT stop() {
        int r = jniStop();
        return FLX_CI_RESULT.toResult(r);
    }

    public static void processAgentMsg(String msg) {
        Context context = EApp.getAppContext();
        Intent intent = new Intent(context, EAppService.class);
        intent.setAction(EAppService.REQ_CI_MSG);
        intent.putExtra("msg", msg);
        //Log.d(TAG, "Got msg: " + msg);
        context.startService(intent);
    }

    public static void processAgentEvent(int event, String msg) {
        Context context = EApp.getAppContext();
        Intent intent = new Intent(context, EAppService.class);
        intent.setAction(EAppService.REQ_CI_EVENT);
        intent.putExtra("event", event);
        intent.putExtra("msg", msg);
        context.startService(intent);
    }

    String getDeviceInfo(String key) {
        if (key.equals("DEVICE_ID")) {
            String value = mController.getDeviceAttribute(StaticEndPoint.EP_ID, key);
            //String value = "359860070022961";
            Log.d(TAG, "Reading DEVICE_ID from ep0:" + value);
            return value;
        }
        else if (key.equals("DEVICE_SN")) {
            String value = mController.getDeviceAttribute(StaticEndPoint.EP_ID, key);
            //String value = "FZAUS17210000M";
            Log.d(TAG, "Reading DEVICE_SN from ep0:" + value);
            return value;
        }
        else {
            return mController.mEappSettings.get(key);
        }
    }

    String getOTAFileDir() {

        File d = new File(extFilesDir + "/OTA/");
        if (!d.exists()) {
            d.mkdir();
        }

        return extFilesDir + "/OTA/";
    }

    String getNVMFilename() {
        return extFilesDir + "/" + "nvm.bin";
    }

    String getNVMAttrFilename() {
        return extFilesDir + "/" + "nvm_attr.bin";
    }

    String getAgentDBFilename() {
        return extFilesDir + "/" + "agent.db";
    }

    String getCertFilename() {
        return filesDir + "/" + "ca-bundle.crt";
    }

    void clearNVMFile() {
        File f = new File(getNVMFilename());
        if (f != null && f.exists()) {
            boolean pass = f.delete();
            Log.d(TAG, "Deleted nvm file:" + pass);
        }
    }

    private native int jniInit();
    public native String jniGetAgentLibVersion();
    public native int jniGetActivationStatus();
    public native int jniActivate();
    public native int jniDeactivate();
    public native int jniSync();
    public native int jniConnect();
    public native int jniDisconnect();
    public native int jniClearActivationData();
    public native int jniSendStringAttributeMsg(int epId, int localId, String value);
    public native int jniSendIntAttributeMsg(int epId, int localId, int value);
    public native int jniSendLongAttributeMsg(int epId, int localId, long value);
    public native int jniSendFloatAttributeMsg(int epId, int localId, float value);
    public native int jniSendBooleanAttributeMsg(int epId, int localId, boolean value);
    public native int jniAddDynamicEndpoint(int epId, String mfgId, String userField, String name);
    public native int jniDeleteDynamicEndpoint(int epId);
    public native int jniStop();

    static {
        System.loadLibrary("native-lib");
    }

}
