package com.flex.eapp;

import android.app.job.JobScheduler;
import android.util.Log;

import com.intel.rmtmgmtprov.api.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */

class EndPoint {
    private static final String TAG = "EndPoint";
    public static final int MIN_DYNAMIC_EP = 100;
    private static int next_dynamic_epId = MIN_DYNAMIC_EP;

    // should actually be in BtEndPoint sub-class, but too lazy create one
    /* TODO: to fix above */
    public static final String BT_MFG_ID = "BT001";

    private int mEpId;
    private HashMap<Integer, Attribute> mAttrs = new HashMap<>();

    EndPoint(int epId) {
        mEpId = epId;
    }

    void addAttribute(Attribute attr) {
        mAttrs.put(attr.getLocalId(), attr);
        //Log.v(TAG, "Add attr localId:" + attr.getLocalId() + " desc:" + attr.getDesc());
    }

    void updateAttribute(int localId, String value) {
        Attribute attr = mAttrs.get(localId);
        if (attr != null) {
            attr.setString(value);
            attr.setUpdated(true);
            //Log.d(TAG, "Update boolean attr id:" + localId + " Desc: " + attr.getDesc() + " Value:" + value);
        }
    }

    void updateAttribute(int localId, Integer value) {
        Attribute attr = mAttrs.get(localId);
        if (attr != null) {
            attr.setInt(value);
            attr.setUpdated(true);
            //Log.d(TAG, "Update boolean attr id:" + localId + " Desc: " + attr.getDesc() + " Value:" + value);
        }
    }

    void updateAttribute(int localId, Long value) {
        Attribute attr = mAttrs.get(localId);
        if (attr != null) {
            attr.setLong(value);
            attr.setUpdated(true);
            //Log.d(TAG, "Update boolean attr id:" + localId + " Desc: " + attr.getDesc() + " Value:" + value);
   }} 
	public synchronized static int getNext_dynamic_epId() {
        return ++next_dynamic_epId;
    }

    void updateAttribute(int localId, Float value) {
        Attribute attr = mAttrs.get(localId);
        if (attr != null) {
            attr.setFloat(value);
            attr.setUpdated(true);
            //Log.d(TAG, "Update boolean attr id:" + localId + " Desc: " + attr.getDesc() + " Value:" + value);
        }
    }

    void updateAttribute(int localId, Boolean value) {
        Attribute attr = mAttrs.get(localId);
        if (attr != null) {
            attr.setBoolean(value);
            attr.setUpdated(true);
            //Log.d(TAG, "Update boolean attr id:" + localId + " Desc: " + attr.getDesc() + " Value:" + value);
        }
    }

    int getEpId() {
        return mEpId;
    }

    Attribute getAttribute(int localId) {
        return mAttrs.get(localId);
    }

    HashMap<Integer, Attribute> getAllAttributes() {
        return mAttrs;
    }

    HashMap<Integer, Attribute> getUpdatedAttributes() {
        HashMap<Integer, Attribute> updatedAttrs = new HashMap<>();
        for (Map.Entry<Integer, Attribute> entry : mAttrs.entrySet()) {
            Attribute attr = entry.getValue();
            if (attr.isUpdated()) {
                updatedAttrs.put(attr.getLocalId(),attr);
            }
        }
        return updatedAttrs;
    }

    class Attribute {
        private int mLocalId;
        private String mDesc;
        private boolean mUpdated;
        private Object mValue;
        private Class<?> mCls;

        Attribute(int localId, String desc, Class<?> cls) {
            mLocalId = localId;
            mDesc = desc;
            mCls = cls;
        }

        int getLocalId() {
            return mLocalId;
        }

        String getDesc() {
            return mDesc;
        }

        boolean isUpdated() {
            return mUpdated;
        }

        void setUpdated(boolean updated) {
            mUpdated = updated;
        }

        Class<?> getValueClass() {
            return mCls;
        }

        private <T> T getValue(Class<T> cls) {
            if (mCls.equals(cls)) {
                return cls.cast(mValue);
            }
            return null;
        }

        String getString() {
            return getValue(String.class);
        }

        Integer getInt() {
            return getValue(Integer.class);
        }

        Long getLong() {
            return getValue(Long.class);
        }

        Float getFloat() {
            return getValue(Float.class);
        }

        Boolean getBoolean() {
            return getValue(Boolean.class);
        }

        private <T> void set(T obj, Class<T> cls) {
            if (mCls.equals(cls)) {
                mValue = obj;
            }
            else {
                Log.e("EndPointAttribute", "Unable to set localID:" + mLocalId
                        + " of type " + mCls.getName() + " with " + obj);
            }
        }

        void setString(String value) {
            set(value, String.class);
        }

        void setInt(Integer value) {
            set(value, Integer.class);
        }

        void setLong(Long value) {
            set(value, Long.class);
        }

        void setFloat(Float value) {
            set(value, Float.class);
        }

        void setBoolean(Boolean value) {
            set(value, Boolean.class);
        }
    }
}

