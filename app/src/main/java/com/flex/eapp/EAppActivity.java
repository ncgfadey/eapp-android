package com.flex.eapp;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

public class EAppActivity extends AppCompatActivity {

    private static final String TAG = "EAppActivity";

    public static final String EXTRA_START_REASON = "com.flex.eapp.mainactivity.start_reason";
    public static final String START_REASON_EXCEPTION = "previous exception";
    public static final String START_REASON_BOOT_COMPLETED = "boot completion";
    public static final String START_REASON_EAPP_UPDATED = "EApp update";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eapp);

        String eappVersion = getString(R.string.eapp);
        Intent intent = getIntent();
        String startReason = intent.getStringExtra(EXTRA_START_REASON);
        if (startReason != null) {
            Log.d(TAG, "Starting " + eappVersion + " because of " + startReason);
        }
        else {
            Log.d(TAG, "Starting " + eappVersion);
        }
        Thread.setDefaultUncaughtExceptionHandler(new EAppExceptionHandler());
        startService();

        new Handler().postDelayed( new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Finishing activity screen");
                finishAndRemoveTask();
            }
        }, 3000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void startService() {
        Intent intent = new Intent(this, EAppService.class);
        intent.setAction(EAppService.REQ_START_SERVICE_FROM_ACTIVITY);
        ComponentName c = getApplicationContext().startService(intent);
    }
}
