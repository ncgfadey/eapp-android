package com.flex.eapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;

import com.intel.rmtmgmtprov.api.Common;
import com.intel.rmtmgmtprov.api.IRmtMgmtProvCBAPI;
import com.intel.rmtmgmtprov.api.RmtMgmtProvAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */

/* Encapsulates interface to Intel's Remote Mgmt and Provisioning
 * (RmtMgmtProv) service. Takes care of checking the service before
 * making requests and restoring connection if necessary.
 */

class RmtMgmt {
    private static final String TAG = RmtMgmt.class.getSimpleName();
    private final String API_VERSION = "1.0";
    private final long REQ_TIMEOUT_DEFAULT_MS = 30000;

    private RmtMgmtProvAPI mApi;
    private boolean mReady;
    private int mMsgId;
    private HashMap<Integer, Request> mPendingRequests = new HashMap<Integer, Request>();
    private final RmtMgmtProvCBAPI mCallback = new RmtMgmtProvCBAPI();


    /* Start RmtMgmtProv.It may be called by isReady() to restart
     * a possibly broken connection before sending a request.
     */
    EAppService.STATUS_CODE start() {
        Context context = EApp.getAppContext();
        mApi = new RmtMgmtProvAPI(context);
        try {
            mApi.startRmtMgmtProv(mCallback);
            Log.d(TAG, "RmtMgmtProv API started");
        } catch (RemoteException re) {
            mApi = null;
            Log.e(TAG, "Unable to start RmtMgmtProv API");
            re.printStackTrace();
            return EAppService.STATUS_CODE.API_REMOTE_EXCEPTION;
        }

        long timeout = 0;
        long interval = 1000;
        while (!isReady() && timeout < (REQ_TIMEOUT_DEFAULT_MS*2)) {
            SystemClock.sleep(interval);
            Log.d(TAG, "Waiting for connect: " + timeout + "ms");
            timeout += interval;
        }
        /* start() may be called multiple times. In that case we
         * check readiness.
         */
        if (isReady()) {
            Log.d(TAG, "RmtMgmtProv API ready");
            return EAppService.STATUS_CODE.SUCCESS;
        }
        return EAppService.STATUS_CODE.NOT_READY;
    }

    void stop() {
        if (mApi != null) {
            try {
                mApi.stopRmtMgmtProv();
            } catch (RemoteException re) {
                Log.e(TAG, "Unable to stop RmtMgmtProv API");
                re.printStackTrace();
            }
            mApi = null;
        }
        setReady(false); /* manually set ready to false becasue we can no longer get events */
        Log.d(TAG, "RmtMgmtProv API stopped");
    }

    /* Check if Intel service is ready to respond. Intel service
     * could die and get restarted on us. We check on every request.
     */
    synchronized boolean isReady() {
        return mReady;
    }

    private synchronized void setReady(boolean ready) {
        mReady = ready;
    }

    private boolean checkRmtMgmtProv() {
        EAppService.STATUS_CODE status;

        /* On current build, checking readiness involves a sending an initial
         * command and verifying a response. In the future, this could be a noop.
         * Instead we would *synchronized poll* for a "ready" event from
         * RmtMgmtProv service.
         */
        Log.d(TAG, "Check RmtMgmtProv...");
        /* The request timeout is set to 500ms because this test may called
         * from UI thread.
         */
        Request req = new Request(Common.REQUEST_GET_DATE_TIME, 500);
        status = req.send(false); /* Do not check readiness before sending */
        if (status != EAppService.STATUS_CODE.SUCCESS) {
            Log.e(TAG, "RmtMgmtProv not ready: " + status);
            return false;
        }
        return true;
    }



    private class RmtMgmtProvCBAPI implements IRmtMgmtProvCBAPI {
        @Override
        public void response(int msgId, JSONObject response) {
            Request req = mPendingRequests.get(msgId);
            if (req == null) {
                Log.e(TAG, "Unable to find matching msgId " + msgId);
                if (response != null) {
                    Log.e(TAG, "Unmatched response JSON: " + response.toString());
                }
                return;
            }
            req.setResponse(response);
            req.wakeUp();
        }

        @Override
        public void event(final JSONObject event) {
            try {
                String eventType = event.getString(Common.PARAM_EVENT_TYPE);
                Log.d(TAG, "Got event " + eventType);
                switch (eventType) {
                    case "RMPService Connected":
                        setReady(true);
                        break;
                    case "RMPService Disconnected":
                        setReady(false);
                        break;
                    default:
                        EAppService.processRmtMgmtEvent(event);
                }
            }
            catch (JSONException je) {
                Log.e(TAG, "Exception parsing event" + event);
                je.printStackTrace();
            }
        }
    }

    private synchronized int getNextMsgId() {
         return mMsgId++;
    }

    /* Creates request sent to RmtMgmtProv and parses the response.
     */
    class Request {
        private int mRequestId = getNextMsgId();
        private String mAction;
        private JSONObject mRequest = new JSONObject();
        private JSONObject mResponse;
        private boolean responseRcvd;
        private long mReqTimeoutMs = REQ_TIMEOUT_DEFAULT_MS;

        Request(String action) {
            mAction = action;
        }

        Request(String action, long reqTimeoutMs) {
            this(action);
            mReqTimeoutMs = reqTimeoutMs;
        }

        EAppService.STATUS_CODE putParam(String key, Object value) {
            try {
                mRequest.put(key, value);
            } catch (JSONException je) {
                Log.e(TAG, "Exception setting request parameter " + key + " to " + value);
                je.printStackTrace();
                return EAppService.STATUS_CODE.JSON_EXCEPTION;
            }
            return EAppService.STATUS_CODE.SUCCESS;
        }

        EAppService.STATUS_CODE putParam(String key, Object[] values) {
            JSONArray jsonArray = new JSONArray();
            try {
                for (Object value : values) {
                    jsonArray.put(value);
                }
                mRequest.put(key, jsonArray);
            } catch (JSONException je) {
                Log.e(TAG, "Exception setting request parameter " + key + " on array");
                je.printStackTrace();
                return EAppService.STATUS_CODE.JSON_EXCEPTION;
            }
            return EAppService.STATUS_CODE.SUCCESS;
        }

        synchronized EAppService.STATUS_CODE send() {
            return send(true);
        }

        private synchronized EAppService.STATUS_CODE send(boolean checkConn) {
            if (checkConn) {
                boolean isReady = isReady();
                if (!isReady) {
                    Log.e(TAG, "Unable to start RmtMgmtProv for " + mAction);
                    return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
                }
            }
            try {
                mRequest.put(Common.PARAM_ACTION, mAction);
                mRequest.put(Common.PARAM_API_VERSION, API_VERSION);
            }
            catch (JSONException je) {
                Log.e(TAG, "Exception setting request action/api to " + mAction);
                je.printStackTrace();
                return EAppService.STATUS_CODE.JSON_EXCEPTION;
            }

            try {
                Common.RESULT_CODE result = mApi.request(mRequestId, mRequest);
                if (result != Common.RESULT_CODE.SUCCESS) {
                    Log.e(TAG, "Error sending request for " + mAction + " : " + result);
                    return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
                }
            } catch (RemoteException re) {
                Log.e(TAG, "Exception sending request for " + mAction);
                re.printStackTrace();
                return EAppService.STATUS_CODE.API_REMOTE_EXCEPTION;
            }

            boolean waitTimeout = false;
            mPendingRequests.put(mRequestId, this);
            while (!responseRcvd) {
                try {
                    wait(mReqTimeoutMs);
                    if (!responseRcvd) {
                        waitTimeout = true;
                        break;
                    }
                } catch (InterruptedException ie) {
                    Log.e(TAG, "Interrupted while waiting for " + mAction + " request to complete");
                    ie.printStackTrace();
                }
            }

            if (waitTimeout) {
                Log.e(TAG, "Timeout waiting for request:" + mAction);
                return EAppService.STATUS_CODE.TIMEOUT;
            }

            try {
                int result = mResponse.getInt(Common.PARAM_RESULT);
                Log.d(TAG, "Req " + mRequestId + " status. Result:" + mResponse);
                if (result != 0) {
                    String desc = mResponse.getString(Common.PARAM_DESCRIPTION);
                    Log.e(TAG, "Req " + mRequestId + " failed. Result:" + result + " Desc:" + desc);
                    return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
                }
            } catch (JSONException je) {
                Log.e(TAG, "Exception parsing response to action " + mAction);
                je.printStackTrace();
                return EAppService.STATUS_CODE.JSON_EXCEPTION;
            }
            return EAppService.STATUS_CODE.SUCCESS;
        }

        private synchronized void wakeUp() {
            responseRcvd = true;
            notify();
        }

        String getAction() {
            return mAction;
        }

        void setResponse(JSONObject response) {
            mResponse = response;
        }

        JSONObject getResponse() {
            return mResponse;
        }
    }

}
