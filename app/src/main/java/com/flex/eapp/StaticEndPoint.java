package com.flex.eapp;


import android.util.Log;

import com.intel.rmtmgmtprov.api.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */

class StaticEndPoint extends EndPoint {
    private final String TAG = this.getClass().getSimpleName();

    /* Static endpoint always has ID 0 */
    public static final int EP_ID = 0;

    /* Attribute IDs */
    public static final int LOCAL_ID_ANDROID_ID                 = 1;
    public static final int LOCAL_ID_BT_MAC_ADDR                = 3;
    public static final int LOCAL_ID_AVAIL_MEM_DEVICE           = 5;
    public static final int LOCAL_ID_NUM_PAIRED_DEVICES         = 8;
    public static final int LOCAL_ID_SIM_STATE                  = 10;
    public static final int LOCAL_ID_WIFI_RSSI                  = 12;
    public static final int LOCAL_ID_SIM_NO                     = 13;
    public static final int LOCAL_ID_OPERATOR_NAME              = 14;
    public static final int LOCAL_ID_ROAMING_STATE              = 15;
    public static final int LOCAL_ID_WIFI_SSID                  = 16;
    public static final int LOCAL_ID_GATEWAY_DATE               = 18;
    public static final int LOCAL_ID_LOW_MEMORY                 = 20;
    public static final int LOCAL_ID_TOTAL_MEMORY               = 21;
    public static final int LOCAL_ID_VM_FREE_MEMORY             = 22;
    public static final int LOCAL_ID_VM_MAX_MEMORY              = 23;
    public static final int LOCAL_ID_VM_TOTAL_MEMORY            = 24;
    public static final int LOCAL_ID_HEAP                       = 25;
    public static final int LOCAL_ID_MOBILE_BYTES_RCVD          = 26;
    public static final int LOCAL_ID_MOBILE_BYTES_SENT          = 27;
    public static final int LOCAL_ID_WIFI_BYTES_RCVD            = 28;
    public static final int LOCAL_ID_WIFI_BYTES_SENT            = 29;
    public static final int LOCAL_ID_BT_SCAN_MODE               = 30;
    public static final int LOCAL_ID_BT_STATE                   = 31;
    public static final int LOCAL_ID_DEVICE_UPTIME              = 32;
    public static final int LOCAL_ID_DEVICE_AWAKE_UPTIME        = 33;
    public static final int LOCAL_ID_DEVICE_CPU_USAGE           = 34;
    public static final int LOCAL_ID_STORAGE_DIR_PATH           = 35;
    public static final int LOCAL_ID_STORAGE_BLOCK_SIZE         = 36;
    public static final int LOCAL_ID_STORAGE_TOTAL_BLOCKS_COUNT = 37;
    public static final int LOCAL_ID_STORAGE_AVAIL_BLOCKS_COUNT = 38;
    public static final int LOCAL_ID_APP_LIST                   = 40;
    public static final int LOCAL_ID_GATEWAY_TIMEZONE           = 41;
    public static final int LOCAL_ID_GATEWAY_AUTO_TIME_MODE     = 42;
    public static final int LOCAL_ID_GATEWAY_AUTO_TIMEZONE_MODE = 43;
    public static final int LOCAL_ID_CONFIG_LOG_FILES           = 45;

    StaticEndPoint() {
        super(EP_ID);

        addAttribute(new Attribute(LOCAL_ID_ANDROID_ID, "Android ID", String.class));
        addAttribute(new Attribute(LOCAL_ID_BT_MAC_ADDR, "BT MAC Address", String.class));
        addAttribute(new Attribute(LOCAL_ID_AVAIL_MEM_DEVICE, "Available Memory Device", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_NUM_PAIRED_DEVICES, "Number of Paired Devices", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_SIM_STATE, "SIM State", String.class));
        addAttribute(new Attribute(LOCAL_ID_WIFI_RSSI, "WiFi RSSI", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_SIM_NO, "Sim Number", String.class));
        addAttribute(new Attribute(LOCAL_ID_OPERATOR_NAME, "Operator Name", String.class));
        addAttribute(new Attribute(LOCAL_ID_ROAMING_STATE, "Roaming State", Boolean.class));
        addAttribute(new Attribute(LOCAL_ID_WIFI_SSID, "WiFi SSID", String.class));
        addAttribute(new Attribute(LOCAL_ID_GATEWAY_DATE, "Gateway Date", String.class));
        addAttribute(new Attribute(LOCAL_ID_LOW_MEMORY, "Low Memory", Boolean.class));
        addAttribute(new Attribute(LOCAL_ID_TOTAL_MEMORY, "Total Memory", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_VM_FREE_MEMORY, "VM Free Memory", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_VM_MAX_MEMORY, "VM Max Memory", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_VM_TOTAL_MEMORY, "VM Total Memory", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_HEAP, "Heap", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_MOBILE_BYTES_RCVD, "Mobile Bytes Received", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_MOBILE_BYTES_SENT, "Mobile Bytes Sent", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_WIFI_BYTES_RCVD, "WiFi Bytes Received", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_WIFI_BYTES_SENT, "WiFi Bytes Sent", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_BT_SCAN_MODE, "BT Scan Mode", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_BT_STATE, "BT State", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_DEVICE_UPTIME, "Device Uptime", Long.class));
        addAttribute(new Attribute(LOCAL_ID_DEVICE_AWAKE_UPTIME, "Device Awake Uptime", Long.class));
        addAttribute(new Attribute(LOCAL_ID_DEVICE_CPU_USAGE, "Device CPU Usage", Float.class));
        addAttribute(new Attribute(LOCAL_ID_STORAGE_DIR_PATH, "Storage Directory Path", String.class));
        addAttribute(new Attribute(LOCAL_ID_STORAGE_BLOCK_SIZE, "Storage Block Size", Long.class));
        addAttribute(new Attribute(LOCAL_ID_STORAGE_TOTAL_BLOCKS_COUNT, "Storage Total Blocks Count", Long.class));
        addAttribute(new Attribute(LOCAL_ID_STORAGE_AVAIL_BLOCKS_COUNT, "Storage Available Blocks Count", Long.class));
        addAttribute(new Attribute(LOCAL_ID_APP_LIST, "AppList", String.class));
        addAttribute(new Attribute(LOCAL_ID_GATEWAY_TIMEZONE, "Gateway TimeZone", String.class));
        addAttribute(new Attribute(LOCAL_ID_GATEWAY_AUTO_TIME_MODE, "Gateway Auto Time Mode", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_GATEWAY_AUTO_TIMEZONE_MODE, "Gateway Auto TimeZone Mode", Integer.class));
        addAttribute(new Attribute(LOCAL_ID_CONFIG_LOG_FILES, "Config/Log Files", String.class));
    }

    DeviceInfo mDeviceInfo = new DeviceInfo();

    /* Give deviceInfo read from Intel service, parse and update
     * static endpoint attributes. If updated, mark the attribute
     * as needing a sync to cloud
     */
    void updateAttributes(JSONObject deviceInfo, String filter) {

        mDeviceInfo.parseDeviceInfo(deviceInfo, filter);

        DeviceInfo.Identifiers identifierInfo = mDeviceInfo.getIdentifiers();
        if (identifierInfo.mUpdated) {
            updateAttribute(LOCAL_ID_ANDROID_ID, identifierInfo.mAndroidId);
            updateAttribute(LOCAL_ID_BT_MAC_ADDR, identifierInfo.mBTMacAddr);
            identifierInfo.mUpdated = false;
        }

        DeviceInfo.SimInfo simInfo = mDeviceInfo.getSimInfo();
        if (simInfo.mUpdated) {
            updateAttribute(LOCAL_ID_SIM_STATE, simInfo.mState);
            updateAttribute(LOCAL_ID_SIM_NO, simInfo.mSimNumber);
            updateAttribute(LOCAL_ID_OPERATOR_NAME,  simInfo.mOperatorName);
            updateAttribute(LOCAL_ID_ROAMING_STATE, simInfo.mIsRoaming);
            simInfo.mUpdated = false;
        }

        DeviceInfo.WifiInfo wifiInfo = mDeviceInfo.getWifiInfo();
        if (wifiInfo.mUpdated) {
            updateAttribute(LOCAL_ID_WIFI_RSSI, wifiInfo.mRssi);
            updateAttribute(LOCAL_ID_WIFI_SSID, wifiInfo.mSsid);
            wifiInfo.mUpdated = false;
        }

        DeviceInfo.BtInfo btInfo = mDeviceInfo.getBtInfo();
        if (btInfo.mUpdated) {
            updateAttribute(LOCAL_ID_NUM_PAIRED_DEVICES, btInfo.mPairedDevices.size());
            updateAttribute(LOCAL_ID_BT_SCAN_MODE, btInfo.mScanMode);
            updateAttribute(LOCAL_ID_BT_STATE, btInfo.mState);
            btInfo.mUpdated = false;
        }

        DeviceInfo.MemInfo memInfo = mDeviceInfo.getMemInfo();
        if (memInfo.mUpdated) {
            updateAttribute(LOCAL_ID_AVAIL_MEM_DEVICE, memInfo.mAvailMem);
            updateAttribute(LOCAL_ID_LOW_MEMORY, memInfo.mLowMem);
            updateAttribute(LOCAL_ID_TOTAL_MEMORY, memInfo.mTotalMem);
            updateAttribute(LOCAL_ID_VM_FREE_MEMORY, memInfo.mVmFreeMem);
            updateAttribute(LOCAL_ID_VM_MAX_MEMORY, memInfo.mVmMaxMem);
            updateAttribute(LOCAL_ID_VM_TOTAL_MEMORY, memInfo.mVmTotalMem);
            updateAttribute(LOCAL_ID_HEAP, memInfo.mHeapSize);
            memInfo.mUpdated = false;
        }

        DeviceInfo.DataUsage dataUsage = mDeviceInfo.getDataUsage();
        if (dataUsage.mUpdated) {
            updateAttribute(LOCAL_ID_MOBILE_BYTES_RCVD, dataUsage.mMobileBytesReceived);
            updateAttribute(LOCAL_ID_MOBILE_BYTES_SENT, dataUsage.mMobileBytesSent);
            updateAttribute(LOCAL_ID_WIFI_BYTES_RCVD, dataUsage.mWifiBytesReceived);
            updateAttribute(LOCAL_ID_WIFI_BYTES_SENT, dataUsage.mWifiBytesSent);
            dataUsage.mUpdated = false;
        }

        DeviceInfo.DevInfo devInfo = mDeviceInfo.getDevInfo();
        if (devInfo.mUpdated) {
            updateAttribute(LOCAL_ID_DEVICE_UPTIME, devInfo.mUptime);
            updateAttribute(LOCAL_ID_DEVICE_AWAKE_UPTIME, devInfo.mAwakeUptime);
            updateAttribute(LOCAL_ID_DEVICE_CPU_USAGE, devInfo.mCpuUsage);
            devInfo.mUpdated = false;
        }

        DeviceInfo.StorageInfo storageInfo = mDeviceInfo.getStorageInfo();
        if (storageInfo.mUpdated) {
            if (!storageInfo.paths.isEmpty()) {
                updateAttribute(LOCAL_ID_STORAGE_DIR_PATH, storageInfo.paths.get(0).mDirPath);
                updateAttribute(LOCAL_ID_STORAGE_BLOCK_SIZE, storageInfo.paths.get(0).mBlockSize);
                updateAttribute(LOCAL_ID_STORAGE_TOTAL_BLOCKS_COUNT, storageInfo.paths.get(0).mTotalCount);
                updateAttribute(LOCAL_ID_STORAGE_AVAIL_BLOCKS_COUNT, storageInfo.paths.get(0).mAvailCount);
            }
            storageInfo.mUpdated = false;
        }
    }

    class DeviceInfo {
        void parseDeviceInfo(JSONObject deviceInfo, String filter) {
            parseIdentifierInfo(deviceInfo);
            if (filter.equals(Common.VAL_FILTER_ALL)) {
                parseSimInfo(deviceInfo);
                parseWifiInfo(deviceInfo);
                parseBtInfo(deviceInfo);
                parseMemInfo(deviceInfo);
                parseDataUsage(deviceInfo);
                parseStorageInfo(deviceInfo);
                parseDevInfo(deviceInfo);
            }
        }

        Identifiers mIdentifiers = new Identifiers();

        class Identifiers {
            String mSerialNumber;
            String mAndroidId;
            String mIMEINumber;
            String mBTMacAddr;
            String mWiFiMacAddr;
            boolean mUpdated;
        }

        Identifiers getIdentifiers() {
            return mIdentifiers;
        }

        boolean parseIdentifierInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_IDENTIFIERS);

                mIdentifiers.mSerialNumber = json.getString(Common.PARAM_SERIAL_NUMBER);
                mIdentifiers.mAndroidId = json.getString(Common.PARAM_ANDROID_ID);
                mIdentifiers.mIMEINumber = json.getString(Common.PARAM_IMEI_Number);
                mIdentifiers.mBTMacAddr = json.getString(Common.PARAM_BT_ADDRESS);
                mIdentifiers.mWiFiMacAddr = json.getString(Common.PARAM_WIFI_ADDRESS);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mIdentifiers.mUpdated = true;
            return true;
        }

        SimInfo mSimInfo = new SimInfo();

        class SimInfo {
            boolean mIsRoaming;
            String mSimNumber;
            String mOperatorName;
            String mState;
            boolean mUpdated;
        }

        SimInfo getSimInfo() {
            return mSimInfo;
        }

        boolean parseSimInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_SIM_INFO);
                mSimInfo.mIsRoaming = json.getBoolean(Common.PARAM_SIM_IS_ROAMING);
                mSimInfo.mSimNumber = json.getString(Common.PARAM_SIM_NUMBER);
                mSimInfo.mOperatorName = json.getString(Common.PARAM_SIM_OPERATOR_NAME);
                mSimInfo.mState = json.getString(Common.PARAM_SIM_STATE);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mSimInfo.mUpdated = true;
            return true;
        }

        WifiInfo mWifiInfo = new WifiInfo();

        class WifiInfo {
            String mSsid;
            int mRssi;
            boolean mUpdated;
        }

        WifiInfo getWifiInfo() {
            return mWifiInfo;
        }

        boolean parseWifiInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_WIFI_INFO);

                mWifiInfo.mSsid = json.getString(Common.PARAM_SSID);
                mWifiInfo.mRssi = json.getInt(Common.PARAM_RSSI);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mWifiInfo.mUpdated = true;
            return true;
        }

        BtInfo mBtInfo = new BtInfo();

        class BtInfo {
            int mScanMode;
            int mState;
            HashMap<String, String> mPairedDevices = new HashMap<>();
            boolean mUpdated;
        }

        BtInfo getBtInfo() {
            return mBtInfo;
        }

        boolean parseBtInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_BT_INFO);

                mBtInfo.mScanMode = json.getInt(Common.PARAM_BT_SCAN_MODE);
                mBtInfo.mState = json.getInt(Common.PARAM_BT_STATE);
                JSONArray pairedDevices = json.getJSONArray(Common.PARAM_BT_PAIRED_DEVICES);
                for (int i = 0; i < pairedDevices.length(); i++) {
                    JSONObject bt = pairedDevices.getJSONObject(i);
                    String btName = bt.getString(Common.PARAM_BT_NAME);
                    String btAddr = bt.getString(Common.PARAM_BT_ADDRESS);
                    mBtInfo.mPairedDevices.put(btName, btAddr);
                }
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mBtInfo.mUpdated = true;
            return true;
        }

        MemInfo mMemInfo = new MemInfo();

        class MemInfo {
            int mAvailMem;
            boolean mLowMem;
            int mTotalMem;
            int mVmFreeMem;
            int mVmMaxMem;
            int mVmTotalMem;
            int mHeapSize;
            boolean mUpdated;
        }

        MemInfo getMemInfo() {
            return mMemInfo;
        }

        boolean parseMemInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_MEM_INFO);

                mMemInfo.mAvailMem = json.getInt(Common.PARAM_AVAILABLE_MEMORY);
                mMemInfo.mLowMem = json.getBoolean(Common.PARAM_LOW_MEMORY);
                mMemInfo.mTotalMem = json.getInt(Common.PARAM_TOTAL_MEMORY);
                mMemInfo.mVmFreeMem = json.getInt(Common.PARAM_VM_FREE_MEMORY);
                mMemInfo.mVmMaxMem = json.getInt(Common.PARAM_VM_MAX_MEMORY);
                mMemInfo.mVmTotalMem = json.getInt(Common.PARAM_VM_TOTAL_MEMORY);
                mMemInfo.mHeapSize = json.getInt(Common.PARAM_HEAP_SIZE);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mMemInfo.mUpdated = true;
            return true;
        }

        DataUsage mDataUsage = new DataUsage();

        class DataUsage {
            int mMobileBytesReceived;
            int mMobileBytesSent;
            int mWifiBytesReceived;
            int mWifiBytesSent;
            boolean mUpdated;
        }

        DataUsage getDataUsage() {
            return mDataUsage;
        }

        boolean parseDataUsage(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_DATA_USAGE);

                mDataUsage.mMobileBytesReceived = json.getInt(Common.PARAM_DU_MOBILE_RECEIVED);
                mDataUsage.mMobileBytesSent = json.getInt(Common.PARAM_DU_MOBILE_SENT);
                mDataUsage.mWifiBytesReceived = json.getInt(Common.PARAM_DU_WIFI_RECEIVED);
                mDataUsage.mWifiBytesSent = json.getInt(Common.PARAM_DU_WIFI_SENT);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mDataUsage.mUpdated = true;
            return true;
        }

        DevInfo mDevInfo = new DevInfo();

        class DevInfo {
            long mUptime;
            long mAwakeUptime;
            float mCpuUsage;
            boolean mUpdated;
        }

        DevInfo getDevInfo() {
            return mDevInfo;
        }

        boolean parseDevInfo(JSONObject deviceInfoJson) {
            try {
                JSONObject json = deviceInfoJson.getJSONObject(Common.PARAM_DEVICE_INFO);

                mDevInfo.mUptime = json.getLong(Common.PARAM_UPTIME);
                mDevInfo.mAwakeUptime = json.getLong(Common.PARAM_AWAKE_UPTIME);
                mDevInfo.mCpuUsage = (float)json.getDouble(Common.PARAM_CPU_USAGE);
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mDevInfo.mUpdated = true;
            return true;
        }

        StorageInfo mStorageInfo = new StorageInfo();

        class StorageInfo {
            ArrayList<Path> paths = new ArrayList<>();
            class Path {
                Path(String dirPath, long blockSize, long totalCount, long availCount) {
                    mDirPath = dirPath;
                    mBlockSize = blockSize;
                    mTotalCount = totalCount;
                    mAvailCount = availCount;
                }
                String mDirPath;
                long mBlockSize;
                long mTotalCount;
                long mAvailCount;
            }
            boolean mUpdated;
        }

        StorageInfo getStorageInfo() {
            return mStorageInfo;
        }

        boolean parseStorageInfo(JSONObject deviceInfoJson) {
            try {
                JSONArray json = deviceInfoJson.getJSONArray(Common.PARAM_STORAGE_INFO);

                for (int i = 0; i < json.length(); i++) {
                    JSONObject path = json.getJSONObject(i);
                    String dirPath = path.getString(Common.PARAM_SI_DIR_PATH);
                    long blockSize = path.getLong(Common.PARAM_SI_BLOCK_SIZE);
                    long totalCount = path.getLong(Common.PARAM_SI_TOTAL_COUNT);
                    long availCount = path.getLong(Common.PARAM_SI_AVAIL_COUNT);
                    mStorageInfo.paths.add(mStorageInfo. new Path(dirPath,
                            blockSize,
                            totalCount,
                            availCount));
                }
            } catch (JSONException je) {
                je.printStackTrace();
                return false;
            }
            mStorageInfo.mUpdated = true;
            return true;
        }
    }
}
