package com.flex.eapp;


import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.HashMap;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/**
 * Created by Femi.Adeyemi@flex.com
 */

/* EAppJobService - Uses job manager to run EApp jobs based on criteria
 * specified by EAppController, such as network connectivity
 */
public class EAppJobService extends JobService {
    private static final String TAG = EAppJobService.class.getSimpleName();
    private EAppService mService;
    private int tag;
    private HashMap<Integer, EAppServiceConnection> jobs = new HashMap<>();

    @Override
    public boolean onStartJob(JobParameters params) {
        int jobId = params.getJobId();
        EAppServiceConnection conn = new EAppServiceConnection(params);

        Log.d(TAG, "Starting job id:" + jobId + " task id:" + params.getExtras().getInt("taskId"));

        Intent intent = new Intent(this, EAppService.class);
        if (!bindService(intent, conn, Context.BIND_AUTO_CREATE)) {
            Log.e(TAG, "Unable to bind jobid:" + jobId + " to EAppService");
            jobFinished(params, false);
            return false;
        }
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        int jobId = params.getJobId();
        int taskId = params.getExtras().getInt("taskId");
        switch (jobId) {
            case EAppJob.JOB_ID_CONNECT:
                if (!mService.getController().isConnected()) {
                    Log.d(TAG, "Connect jobId:"+ jobId + " did not complete. Restarting...");
                    return true;
                }
                break;
            case EAppJob.JOB_ID_SYNC:
                    return true; /* Ensure we can complete this job and kick off another */
            default:
                Log.d(TAG, "Unknown jobId:"+ jobId);
                break;
        }

        Log.d(TAG, "JobId:"+ jobId + " stopped");
        EAppServiceConnection conn = (EAppServiceConnection)jobs.get(taskId);
        if (conn != null) {
            unbindService(conn);
        }
        return false;
    }

    void handleJob(JobParameters params) {
        EAppController.EAppControllerHandler handler = mService.getHandler();
        Message msg = handler.obtainMessage();
        boolean sendMessage = false;
        int jobId = params.getJobId();
        int taskId = params.getExtras().getInt("taskId");

        switch (jobId) {
            case EAppJob.JOB_ID_CONNECT:
                msg.what = EAppController.REQ_CONNECT_TO_CLOUD;
                sendMessage = true;
                Log.d(TAG, "Sent connect message to controller");
                break;
            case EAppJob.JOB_ID_SYNC:
                msg.what = EAppController.REQ_SYNC_TO_CLOUD;
                sendMessage = true;
                Log.d(TAG, "Sent sync message to controller");
                break;
            default:
                Log.d(TAG, "Unhandled jobId:"+ jobId);
                break;
        }
        if (sendMessage) {
            handler.sendMessage(msg);
        }
        jobFinished(params, false);
        EAppServiceConnection conn = (EAppServiceConnection)jobs.get(taskId);
        if (conn != null) {
            //Log.d(TAG, "Unbinding connection for taskid:" + taskId);
            unbindService(conn);
        }
    }

    private class EAppServiceConnection implements ServiceConnection {
        private JobParameters mParams;

        EAppServiceConnection(JobParameters params) {
            mParams = params;
        }

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            EAppService.EAppServiceBinder binder = (EAppService.EAppServiceBinder) service;
            mService = binder.getService();
            int taskId = mParams.getExtras().getInt("taskId");
            jobs.put(taskId, this);
            handleJob(mParams);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }
    }
}
