package com.flex.eapp;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Log;

import java.util.List;

/**
 * Created by femi on 8/14/17.
 */

class EAppJob {
    private final static String TAG = EAppJob.class.getSimpleName();
    final static int JOB_ID_CONNECT             = 0;
    final static int JOB_ID_SYNC                = 1;
    final static int JOB_ID_NUM                 = 2;

    private int taskId;

    synchronized void scheduleJob(int jobId) {
        scheduleJob(jobId, 0);
    }

    synchronized void scheduleJob(int jobId, long delay) {
        if (jobId >= JOB_ID_NUM) {
            Log.e(TAG, "Job Id:" + jobId + " is invalid");
            return;
        }

        if (isScheduled(jobId)) {
            Log.e(TAG, "Job id:" + jobId + " is already scheduled");
            return;
        }

        Context context = EApp.getAppContext();
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        PersistableBundle extras = new PersistableBundle();
        extras.putInt("taskId", getNewTaskId());
        JobInfo.Builder b = new JobInfo.Builder(jobId,
                new ComponentName(context, EAppJobService.class))
                .setExtras(extras)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        if (delay != 0) {
            b.setMinimumLatency(delay);
        }

        JobInfo jobInfo = b.build();
        int result = jobScheduler.schedule(jobInfo);
        if (result != JobScheduler.RESULT_SUCCESS) {
            Log.e(TAG, "Unable to schedule job id:" + jobId);
        }
    }

    synchronized void cancelJob(int jobId) {
        if (jobId >= JOB_ID_NUM) {
            Log.e(TAG, "Job Id:" + jobId + " is invalid");
            return;
        }

        if (isScheduled(jobId)) {
            Log.e(TAG, "Job id:" + jobId + " was not scheduled");
            return;
        }

        Context context = EApp.getAppContext();
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(jobId);
        Log.e(TAG, "Job id:" + jobId + " cancelled");
    }

    synchronized static void cancelAllJobs() {
        Context context = EApp.getAppContext();
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancelAll();
        Log.e(TAG, "All jobs cancelled");
    }

    private int getNewTaskId() {
        return taskId++;
    }

    private boolean isScheduled(int jobId) {
        Context context = EApp.getAppContext();
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        boolean jobAlreadyScheduled = false;
        List<JobInfo> jobList =  jobScheduler.getAllPendingJobs(); /* API 23 does not have getPendingJob() */
        for (JobInfo job : jobList) {
            if (job.getId() == jobId) {
                jobAlreadyScheduled = true;
                break;
            }
        }
        return jobAlreadyScheduled;
    }
}
