package com.flex.eapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Build;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by femi on 7/26/17.
 */

class EAppSettings {
    private final static String TAG = "EAppSettings";

    private final String PREFS_EAPP = "eapp-prefs";
    private final String FILENAME_CONFIG = "device-info.json";

    public final String EAPP_PREFS_SYNC_PERIOD_HR = "com.eapp.EAppSettings.PERIOD_HR";
    public final String EAPP_PREFS_OTA_MANIFEST_VER = "DEVICE_SW_VERSION";
    public final String EAPP_PREFS_REBOOT_AFTER_UPDATE = "REBOOT_AFTER_UPDATE";
    public final String EAPP_PREFS_CUID_BEFORE_UPDATE = "CUID_BEFORE_UPDATE";

    private Map<String, String> mSettings = new HashMap<>();

    EAppSettings() {
        Log.d(TAG, "Device fingerprint:" + Build.FINGERPRINT);

        initDeviceSettings();
        initPrefs();
    }

    private void initDeviceSettings() {
        mSettings.put("DEVICE_MAC", "00:00:00:00:00:00");
        mSettings.put("DEVICE_OS", "Android");
        mSettings.put("DEVICE_OS_VERSION", Build.VERSION.RELEASE);
        mSettings.put("DEVICE_API_VERSION", "1.0.0");
        mSettings.put("DEVICE_BATCH", "99");
        mSettings.put("DEVICE_RMA", "false");
        mSettings.put("DEVICE_RMA_DATE", "2014-05-05T04: 00:00.000Z");
        mSettings.put("DEVICE_HW_VERSION", Build.BOARD);

        long buildDateUTC = Build.TIME;
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
        Date d = new Date(buildDateUTC);
        mSettings.put("DEVICE_BUILD_DATE", df.format(d));

        mSettings.put("DEVICE_SN", "00000000000000");
        mSettings.put("DEVICE_SW_VERSION", "1.0.0"); /* TODO: set this from build script */
        mSettings.put("DEVICE_SW_NAME", "EApp");
        mSettings.put("DEVICE_HW_NAME", Build.PRODUCT);
        mSettings.put("DEVICE_STATUS", "0x10"); /*TODO: Does this ever change */
        mSettings.put("DEVICE_ID", "000000000000000");
        mSettings.put("CUSTOMER_UNIQUE_ID", ""); /* TODO: set this from build script */

        String[] abiList = Build.SUPPORTED_ABIS;
        if (abiList.length > 1) {
            mSettings.put("DEVICE_PLATFORM_ARCH", abiList[0]);
        }
        else
        {
            mSettings.put("DEVICE_PLATFORM_ARCH", "x86");
        }

        mSettings.put("DEVICE_AGENT_TYPE", "Android");
    }

    private void initPrefs() {
        mSettings.put(EAPP_PREFS_SYNC_PERIOD_HR, "1");
        /* EAPP_PREFS_OTA_MANIFEST_VER is already set to default by device settings */
    }

    void updateFromFile() {
        updateDeviceSettingsFromFile();
        updatePrefsFromFile();
    }

    private void updateDeviceSettingsFromFile() {
        Context context = EApp.getAppContext();
        AssetManager assetManager = context.getAssets();
        byte[] buffer = null;
        try {
            InputStream is = assetManager.open(FILENAME_CONFIG);
            int size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
        }
        catch (IOException ioe) {
            Log.d(TAG, "Exception opening " + FILENAME_CONFIG);
            ioe.printStackTrace();
            return;
        }

        String str = null;
        try {
            str = new String(buffer, "UTF-8");
            JSONObject json = new JSONObject(str);

            Iterator<String> keys = json.keys();
            while(keys.hasNext()) {
                String key = (String) keys.next();
                mSettings.put(key, (String) json.get(key));
                Log.d(TAG, "Updated device setting:" + key + " to " + mSettings.get(key));
            }
        }
        catch (UnsupportedEncodingException uee) {
            Log.e(TAG, "Exception encoding device info string: " + str);
            uee.printStackTrace();
        }
        catch (JSONException je) {
            Log.e(TAG, "Exception creating JSON object on string: " + str);
            je.printStackTrace();
        }
    }

    private void updatePrefsFromFile() {
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        updatePrefsFromFile(settings, EAPP_PREFS_SYNC_PERIOD_HR);
        updatePrefsFromFile(settings, EAPP_PREFS_OTA_MANIFEST_VER);
        updatePrefsFromFile(settings, EAPP_PREFS_REBOOT_AFTER_UPDATE);
        updatePrefsFromFile(settings, EAPP_PREFS_CUID_BEFORE_UPDATE);
    }

    private void updatePrefsFromFile(SharedPreferences settings, String key) {
        String pref = settings.getString(key, null);
        if (pref != null) {
            mSettings.put(key, pref);
            Log.d(TAG, "Updated eapp pref:" + key + " to " + mSettings.get(key));
        }
    }

    String get(String key) {
        return mSettings.get(key);
    }

    String getOTAManifestVersion() {
        return get(EAPP_PREFS_OTA_MANIFEST_VER);
    }

    boolean setOTAManifestVersion(String version) {
        mSettings.put(EAPP_PREFS_OTA_MANIFEST_VER, version);
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EAPP_PREFS_OTA_MANIFEST_VER, version);
        return editor.commit();
    }

    boolean setRebootAfterUpdateFlag() {
        mSettings.put(EAPP_PREFS_REBOOT_AFTER_UPDATE, "1");
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EAPP_PREFS_REBOOT_AFTER_UPDATE, "1");
        return editor.commit();
    }

    boolean clearRebootAfterUpdateFlag() {
        mSettings.put(EAPP_PREFS_REBOOT_AFTER_UPDATE, "0");
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EAPP_PREFS_REBOOT_AFTER_UPDATE, "0");
        return editor.commit();
    }

    boolean getRebootAfterUpdateFlag() {
        String u = get(EAPP_PREFS_REBOOT_AFTER_UPDATE);
        if (u == null) {
            return false;
        }
        if (u.equals("1")) {
            return true;
        }
        return false;
    }

    boolean setCUIDBeforeUpdate(String cuid) {
        mSettings.put(EAPP_PREFS_CUID_BEFORE_UPDATE, cuid);
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EAPP_PREFS_CUID_BEFORE_UPDATE, cuid);
        return editor.commit();
    }

    String getCUIDBeforeUpdate() {
        String c = get(EAPP_PREFS_CUID_BEFORE_UPDATE);
        return c;
    }

    boolean clearCUIDBeforeUpdate() {
        mSettings.remove(EAPP_PREFS_CUID_BEFORE_UPDATE);
        Context context = EApp.getAppContext();
        SharedPreferences settings = context.getSharedPreferences(PREFS_EAPP,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(EAPP_PREFS_CUID_BEFORE_UPDATE);
        return editor.commit();
    }


}
