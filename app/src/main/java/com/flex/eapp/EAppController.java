package com.flex.eapp;


import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.util.Log;
import com.flex.agentdb.BtDeviceDb;
import com.intel.rmtmgmtprov.api.Common;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/*
 * Created by Femi.Adeyemi@flex.com
 */

/* EAppController - Bulk of business logic resides here. It is
 * separated from service to provide a cleaner interface, but
 * it essentially handles the heavy lifting on behalf of EAppService
 * on EApServiceThread
 */
class EAppController {
    private static final String TAG = EAppController.class.getSimpleName();
    private final int REQ_START_SERVICE_DELAY = 5000;
    private final long REQ_CONNECT_CLOUD_DELAY = 30000;
    private final long REQ_SYNC_TO_CLOUD_INTERVAL = 5 * 60 * 1000L;
    EAppAgent mAgent;
    private StaticEndPoint mStaticEp;
    EAppSettings mEappSettings;
    private EAppJob mEAppJob;

    private static BtDeviceDb mBtDb;

    private EAppService mService;
    private boolean mReady;
    private boolean mConnected;
    private boolean mRebootAfterUpdate;
    private boolean mDecomissioned;
    private int jobId;

    final static int REQ_START_SERVICES      = 0;
    final static int REQ_CONNECT_TO_CLOUD    = 1;
    final static int REQ_SYNC_TO_CLOUD       = 2;
    final static int REQ_CI_MSG              = 3;
    final static int RMT_MGMT_EVENT          = 4;
    final static int REQ_CI_EVENT            = 6;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            EAppService.EAppServiceBinder binder = (EAppService.EAppServiceBinder) service;
            mService = binder.getService();
            EAppControllerHandler handler = mService.getHandler();
            Message msg = handler.obtainMessage(REQ_START_SERVICES);
            handler.sendMessage(msg);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }
    };

    /* Handler for all requests, including starting and stopping
     * components that may be destroyed by OS.
     */
    final class EAppControllerHandler extends Handler {

        EAppControllerHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            EAppService.STATUS_CODE status;

            if (!isReady() && msg.what != REQ_START_SERVICES) {
                Log.e(TAG, "Skipping message:" + msg.what + " because controller not ready");
                return;
            }

            switch (msg.what) {
                case REQ_START_SERVICES: {
                    Log.d(TAG, "Msg: REQ_START_SERVICES");
                    status = reqStartServices();
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        EAppControllerHandler handler = mService.getHandler();
                        Message newMsg = handler.obtainMessage(REQ_START_SERVICES);
                        handler.sendMessageDelayed(newMsg, REQ_START_SERVICE_DELAY);
                        Log.d(TAG, "Services not fully up - Retry in " + REQ_START_SERVICE_DELAY + "ms");
                        break;
                    }

                    mEAppJob.scheduleJob(EAppJob.JOB_ID_CONNECT);
                    break;
                }

                case REQ_CONNECT_TO_CLOUD: {
                    Log.d(TAG, "Msg: REQ_CONNECT_TO_CLOUD");
                    status = reqConnect();
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        setConnected(false);
                        if (status == EAppService.STATUS_CODE.NOT_READY) {
                            Log.d(TAG, "Trying connect again in " + REQ_CONNECT_CLOUD_DELAY + "ms");
                            mEAppJob.scheduleJob(EAppJob.JOB_ID_CONNECT, REQ_CONNECT_CLOUD_DELAY);
                        }
                        break;
                    }

                    setConnected(true);
                    Log.i(TAG, "EApp agent connected");
                    sendUpdatedAttributes(mStaticEp);
                    mEAppJob.scheduleJob(EAppJob.JOB_ID_SYNC, REQ_SYNC_TO_CLOUD_INTERVAL);
                    break;
                }

                case REQ_SYNC_TO_CLOUD: {
                    Log.d(TAG, "Msg: REQ_SYNC_TO_CLOUD");
                    if (!isConnected()) {
                        Log.e(TAG, "SEApp is disconnected. Scheduling a connect...");
                        mEAppJob.scheduleJob(EAppJob.JOB_ID_CONNECT, REQ_CONNECT_CLOUD_DELAY);
                        break;
                    }

                    status = reqSync();
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        Log.e(TAG, "Unable to sync. Rescheduling sync...");
                        /* We could have failed sync because of a network error. Retry
                         * sync instead of connect. Try a connect only if we get an
                         * IOTHUB_CONN_DOWN event. If we try to reconnect without this
                         * event, we could put agent in bad state.
                         */
                        mEAppJob.scheduleJob(EAppJob.JOB_ID_SYNC, REQ_SYNC_TO_CLOUD_INTERVAL/2);
                    }
                    else {
                        Log.e(TAG, "Sync passed. Rescheduling...");
                        mEAppJob.scheduleJob(EAppJob.JOB_ID_SYNC, REQ_SYNC_TO_CLOUD_INTERVAL);
                    }
                    break;
                }

                case REQ_CI_MSG: {
                    int startId = msg.arg1;
                    try {
                        JSONObject ciMsg = new JSONObject((String) msg.obj);
                        int type = ciMsg.getInt("type");
                        if (type == 8) {
                            JSONArray ciData = ciMsg.getJSONArray("data");
                            for (int i = 0; i < ciData.length(); i++) {
                                JSONObject setting = ciData.getJSONObject(i);
                                String name = setting.getString("name");
                                String value = setting.getString("value");
                                handleCISetting(name, value);
                            }
                        } else {
                            JSONObject ciData = ciMsg.getJSONObject("data");
                            JSONArray params = ciData.getJSONArray("param");
                            String cmd = ciData.getString("cmdname");
                            handleCIMsg(cmd, params);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    mService.stopSelf(startId);
                    break;
                }

                case REQ_CI_EVENT: {
                    int startId = msg.arg1;
                    int event = msg.arg2;
                    try {
                        handleCIEvent(event, msg.obj);
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    mService.stopSelf(startId);
                    break;
                }

                case RMT_MGMT_EVENT: {
                    Log.d(TAG, "Handle RMT_MGMT_EVENT");
                    int startId = msg.arg1;
                    try {
                        if (msg.obj != null && msg.obj instanceof String) {
                            String eventJSON = (String) msg.obj;
                            handleRmtMgmtEvent(eventJSON);
                        } else {
                            Log.e(TAG, "No eventData");
                        }
                    } catch (Exception err) {
                        Log.e(TAG, "Invalid JSON object");
                    }
                    mService.stopSelf(startId);
                    break;
                }

                default:
                    Log.d(TAG, "Unhandled message: " + msg.what);
                    break;
            }
        }

        EAppService.STATUS_CODE reqStartServices() {
            EAppService.STATUS_CODE status;
            boolean startNotComplete = false;

            Log.d(TAG, "Starting services...");
            RmtMgmt rmtMgmt = mService.getRmtMgmt();
            if (!rmtMgmt.isReady()) {
                status = rmtMgmt.start();
                if (status != EAppService.STATUS_CODE.SUCCESS) {
                    startNotComplete = true;
                    if (status == EAppService.STATUS_CODE.NOT_READY) {
                        Log.e(TAG, "RmtMgmtnot ready: "+ status);
                    }
                    else {
                        Log.e(TAG, "RmtMgmt failed to start: " + status);
                    }
                }
            }

            EAppController controller = mService.getController();
            if (!controller.isReady()) {
                status = controller.start();
                if (status != EAppService.STATUS_CODE.SUCCESS) {
                    startNotComplete = true;
                    Log.e(TAG, "Controller failed to start: "+ status);
                }
                else
                {
                    Log.d(TAG, "Controller started");
                }
            }

            if (startNotComplete) {
                return EAppService.STATUS_CODE.NOT_READY;
            }
            /* Services are up. Check if a reboot after update is needed */
            if (mRebootAfterUpdate) {
                mEappSettings.clearRebootAfterUpdateFlag();
                try {
                    Log.e(TAG, "Reboot scheduled due to EApp update");
                    CIReboot();
                }
                catch (JSONException je) {
                    /* We on purpose clear reboot flag before calling CIReboot.
                     * We do not want to keep persistent reboot flag if we fail
                     * to reboot here.
                     */
                    Log.e(TAG, "Unable to reboot after updating EApp");
                }
            }
            Log.d(TAG, "Services started succesfully");
            return EAppService.STATUS_CODE.SUCCESS;
        }

        EAppService.STATUS_CODE reqConnect() {
            JSONObject response = mService.RmtMgmt_GetDeviceInfo(Common.VAL_FILTER_ALL);
            mStaticEp.updateAttributes(response, Common.VAL_FILTER_ALL);

            EAppAgent.FLX_CI_RESULT ciResult;

            ciResult = mAgent.init();
            if (ciResult != EAppAgent.FLX_CI_RESULT.OK) {
                Log.e(TAG, "Unable to initialize agent:" + ciResult);
                return EAppService.STATUS_CODE.NOT_READY;
            }

            ciResult = mAgent.getActivationStatus();
            if (ciResult == EAppAgent.FLX_CI_RESULT.NOT_ACTIVATED) {
                ciResult = mAgent.activate();
                if (ciResult != EAppAgent.FLX_CI_RESULT.OK) {
                    if (ciResult == EAppAgent.FLX_CI_RESULT.OTA_REQUIRED) {
                        Log.e(TAG, "EApp status: "  + ciResult);
                        return EAppService.STATUS_CODE.SUCCESS;
                    }
                    Log.e(TAG, "Agent not activated on first run: " +
                            "Make sure device is deactivated in cloud: " + ciResult);
                    return EAppService.STATUS_CODE.NOT_READY;
                }
            }

            ciResult = mAgent.sync();
            if (ciResult ==  EAppAgent.FLX_CI_RESULT.NOT_ACTIVATED) {
                ciResult = mAgent.clearActivationData();
                if (ciResult != EAppAgent.FLX_CI_RESULT.OK) {
                    Log.e(TAG, "Unable to clear activation data: " + ciResult);
                    return EAppService.STATUS_CODE.AGENT_ERROR;
                }
                ciResult = mAgent.activate();
                if (ciResult != EAppAgent.FLX_CI_RESULT.OK) {
                    if (ciResult == EAppAgent.FLX_CI_RESULT.OTA_REQUIRED) {
                        Log.e(TAG, "EApp status: "  + ciResult);
                        return EAppService.STATUS_CODE.SUCCESS;
                    }
                    Log.e(TAG, "Unable to activate after clearing: " + ciResult);
                    return EAppService.STATUS_CODE.NOT_READY;
                }
            }
            if (ciResult == EAppAgent.FLX_CI_RESULT.OTA_REQUIRED) {
                Log.e(TAG, "EApp status: "  + ciResult);
                return EAppService.STATUS_CODE.SUCCESS;
            }

            ciResult =  mAgent.connect();
            if (ciResult != EAppAgent.FLX_CI_RESULT.OK) {
                Log.e(TAG, "Unable to connect gateway: " + ciResult);
                return EAppService.STATUS_CODE.NOT_READY;
            }
            return EAppService.STATUS_CODE.SUCCESS;
        }

        EAppService.STATUS_CODE reqSync() {
            EAppAgent.FLX_CI_RESULT ciResult =  mAgent.sync();
            Log.i(TAG, "Sync result: " + ciResult);
            if (ciResult != EAppAgent.FLX_CI_RESULT.OK  &&
                    ciResult != EAppAgent.FLX_CI_RESULT.ALREADY_ACTIVATED ) {
                return EAppService.STATUS_CODE.NOT_READY;
            }
            return EAppService.STATUS_CODE.SUCCESS;
        }

        void handleCIMsg(String cmd, JSONArray params) throws JSONException {
            Log.d(TAG, "handleCIMsg:" + cmd + " params:" + params);
            if (cmd.equals("reqListPackages")) {
                CIListPackages();
            } else if (cmd.equals("reqStartPackages")) {
                CIStartPackages(params);
            }
            else if (cmd.equals("reqStopPackages")) {
                CIStopPackages(params);
            }
            else if (cmd.equals("reqGetDateTime")) {
                CIGetDateTime();
            }
            else if (cmd.equals("reqSetDateTime")) {
                CISetDateTime(params);
            }
            else if (cmd.equals("reqSetDateTime")) {
                CISetDateTime(params);
            }
            else if (cmd.equals("reqTimeZone")) {
                CISetTimeZone(params);
            }
            else if (cmd.equals("reqGetDeviceInfo")) {
                String filter = Common.VAL_FILTER_IDENTIFIERS_ONLY;
                JSONObject param = params.getJSONObject(0); /*TODO: walk array */
                String name = param.getString("name");
                String type = param.getString("type");
                String value = param.getString("value");
                if (name.equals("Filter")) {
                    filter = value;
                    if (filter.equals("indentifiersOnly")) { /* fix typo in cloud */
                        filter = Common.VAL_FILTER_IDENTIFIERS_ONLY;
                    }
                }
                JSONObject r = mService.RmtMgmt_GetDeviceInfo(filter);
                if (r == null) {
                    Log.d(TAG, "command " + cmd + " failed unexpectedly");
                    return;
                }

                mStaticEp.updateAttributes(r, filter);
                sendUpdatedAttributes(mStaticEp);
            }
            else if (cmd.equals("getLogFile")) {
                JSONObject param = params.getJSONObject(0); /*TODO: walk array */
                String name = param.getString("name");
                String type = param.getString("type");
                String value = param.getString("value");
                if (name.equals("filepath") &&
                        type.equals("6")) {
                    CIGetLogFile(value);
                }

            }
            else if (cmd.equals("Reboot")) {
                CIReboot();
            }
            else if (cmd.equals("install")) {
                CIInstallPackages(params);
            }
            else if (cmd.equals("uninstall")) {
                /* TODO - check UI*/
            }
            else if (cmd.equals("provision")) {
                //CISetTimeZone(params);
            }
        }

        void handleCISetting(String name, String value) {
            if (name.equals("Sync Frequency (hours)")) {
                Log.d(TAG, "Setting freq to " + value);
            }
        }

        void handleRmtMgmtEvent(String msg) {
            try {
                JSONObject jObj = new JSONObject(msg);
                RmtMgmtEvent rmtMgmtEvent = RmtMgmtEvent.getEventObject(jObj);
                Log.d(TAG, "Handle "+rmtMgmtEvent.toString());
                switch ( rmtMgmtEvent.getEventType() ) {
                    case BT_PAIRED: {
                        BtEvent btEvent = (BtEvent) rmtMgmtEvent;
                        String btMacAddr = btEvent.getmBtMacAddr();
                        String btName = btEvent.getmBtName();
                        if (mBtDb.findByMac(btMacAddr) == -1) {
                            int epId = EndPoint.getNext_dynamic_epId();
                            EAppAgent.FLX_CI_RESULT flx_result = mAgent.addDynamicEndpoint(epId, EndPoint.BT_MFG_ID, btMacAddr,
                                    btName);
                            if ( (flx_result == EAppAgent.FLX_CI_RESULT.OK)
                                    || (flx_result == EAppAgent.FLX_CI_RESULT.ENDPOINT_ALREADY_EXISTS) ) {
                                mBtDb.insert(epId, btMacAddr, btName);
                                Log.i(TAG, "Added " + btName + "mac:" + btMacAddr);
                            }

                        }
                    }
                    break;

                    case BT_UNPAIRED: {
                        BtEvent btEvent = (BtEvent) rmtMgmtEvent;
                        String btMacAddr = btEvent.getmBtMacAddr();
                        String btName = btEvent.getmBtName();
                        int epId = mBtDb.getLocalIdforBtMac(btMacAddr);
                        if( epId != -1 ) {
                            EAppAgent.FLX_CI_RESULT flx_result = mAgent.deleteDynamicEndpoint(epId);
                            if( (flx_result == EAppAgent.FLX_CI_RESULT.OK)
                                    || (flx_result == EAppAgent.FLX_CI_RESULT.ENDPOINT_NOT_FOUND)) {
                                mBtDb.deleteByLocalId(epId);
                                Log.i(TAG, "Removed " + btName + "mac:" + btMacAddr);
                            }
                        }
                    }
                    break;

                    case CALL_HOME: {
                        EAppAgent.FLX_CI_RESULT ciResult = mAgent.sync();
                        Log.i(TAG, "Call home sync");
                    }
                    break;

                    case DECOMMISIONED: {
                        /* Disconnect and deactivate device. Before disconnecting,
                         * set connected to false so that we do not attempt another
                         * disconnect on the IOTHUB_CONN_DOWN event. Also set
                         * decomission flag so that we do not attempt a reconnect
                         * on the same IOTHUB_CONN_DOWN event
                         */
                        Log.i(TAG, "Stopping EApp due to decommisioning");
                        setConnected(false);
                        setDecommisioned(true);
                        mAgent.disconnect();
                        mAgent.deactivate();
                        // remove BT database
                        mBtDb.close();
                        mBtDb.deleteDbFile();

                        // clears NVM activation record
                        mAgent.clearActivationData();
                        mAgent.stop();

                        /* Similarly, stop messages from reaching handler
                         * before tearing down further
                         */
                        setReady(false);
                        unbindFromService();
                    }
                    break;

                    default:
                        Log.e(TAG, "?Invalid RmtMgmtEvent.EventType?");
                }
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
    }

    EAppService.STATUS_CODE CIListPackages() throws JSONException {
        JSONObject response = mService.RmtMgmt_ListPackages();
        if (response == null) {
            Log.e(TAG, "Error listing packages");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }
        StringBuilder strBuilder = new StringBuilder();
        JSONArray pkgs = response.getJSONArray(Common.PARAM_PACKAGES);
        for (int i = 0; i < pkgs.length(); i++) {
            JSONObject pkg = pkgs.getJSONObject(i);
            String name = pkg.getString(Common.PARAM_PKG_NAME);
            boolean running = pkg.getBoolean(Common.PARAM_PKG_RUNNING);
            strBuilder.append("Name:" + name + " Runing:" + running + "\n");
        }
        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_APP_LIST, pkgs.toString());
        sendUpdatedAttributes(mStaticEp);
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIStartPackages(JSONArray params) throws JSONException {
        if (params.length() == 0) {
            return EAppService.STATUS_CODE.INVALID_PARAMETER;
        }

        String[] pkgs = new String[params.length()];
        for (int i = 0; i < params.length(); i++) {
            pkgs[i] = params.getJSONObject(i).getString("value");
        }

        JSONObject response = mService.RmtMgmt_StartPackages(pkgs);
        if (response == null) {
            Log.e(TAG, "Error starting packages");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }

        JSONArray pkgStatusArr = response.getJSONArray(Common.PARAM_PKG_STATUS);
        for (int i = 0; i < pkgStatusArr.length(); i++) {
            JSONObject pkgStatus = pkgStatusArr.getJSONObject(i);
            String pkgName = pkgStatus.getString(Common.PARAM_PKG_NAME);
            String status = pkgStatus.getString(Common.PARAM_STATUS);
            Log.e(TAG, "App:" + pkgName + " status:" + status);
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIStopPackages(JSONArray params) throws JSONException {
        if (params.length() == 0) {
            return EAppService.STATUS_CODE.INVALID_PARAMETER;
        }

        String[] pkgs = new String[params.length()];
        for (int i = 0; i < params.length(); i++) {
            pkgs[i] = params.getJSONObject(i).getString("value");
        }

        JSONObject response = mService.RmtMgmt_StopPackages(pkgs);
        if (response == null) {
            Log.e(TAG, "Error stopping packages");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }

        JSONArray pkgStatusArr = response.getJSONArray(Common.PARAM_PKG_STATUS);
        for (int i = 0; i < pkgStatusArr.length(); i++) {
            JSONObject pkgStatus = pkgStatusArr.getJSONObject(i);
            String pkgName = pkgStatus.getString(Common.PARAM_PKG_NAME);
            String status = pkgStatus.getString(Common.PARAM_STATUS);
            Log.e(TAG, "App:" + pkgName + " status:" + status);
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIGetDateTime() throws JSONException {
        JSONObject response = mService.RmtMgmt_GetDateTime();
        if (response == null) {
            Log.e(TAG, "Error getting date/time");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }

        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_GATEWAY_DATE,
                                  response.getString(Common.PARAM_DATE_TIME));
        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_GATEWAY_TIMEZONE,
                                  response.getString(Common.PARAM_TIME_ZONE));
        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_GATEWAY_AUTO_TIME_MODE,
                                  response.getInt(Common.PARAM_AUTO_TIME));
        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_GATEWAY_AUTO_TIMEZONE_MODE,
                                  response.getInt(Common.PARAM_AUTO_TIME_ZONE));

        sendUpdatedAttributes(mStaticEp);
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CISetDateTime(JSONArray params) throws JSONException {
        if (params.length() != 1) {
            return EAppService.STATUS_CODE.INVALID_PARAMETER;
        }

        JSONObject param = params.getJSONObject(0);
        String dateTime = param.getString("value");

        JSONObject response = mService.RmtMgmt_SetDateTime(dateTime, null);
        if (response == null) {
            Log.e(TAG, "Error setting date/time");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CISetTimeZone(JSONArray params) throws JSONException {
        if (params.length() != 1) {
            return EAppService.STATUS_CODE.INVALID_PARAMETER;
        }

        JSONObject param = params.getJSONObject(0);
        String timeZone = param.getString("value");

        JSONObject response = mService.RmtMgmt_SetDateTime(null, timeZone);
        if (response == null) {
            Log.e(TAG, "Error setting time zone");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIReboot() throws JSONException {
        /* TODO: we need to do some cleanup here or in service */
        JSONObject response = mService.RmtMgmt_Reboot();
        if (response == null) {
            Log.e(TAG, "Error rebooting");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIGetLogFile(String logFile) throws JSONException {
        String ioErrStr = null;

        StringBuilder logStr = new StringBuilder();
        File f = new File(logFile);

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));

            String line = null;
            while ((line = br.readLine()) != null) {
                logStr.append(line);
            }
        }
        catch (IOException ioe) {
            Log.d(TAG, "Exception opening " + logFile);
            ioErrStr = ioe.toString();
        }

        JSONObject response = new JSONObject();

        if( ioErrStr == null ) {
            Log.d(TAG, "Read log:" + logStr.toString());

            int allowedLen = 1000;
            response.put("fileName", logFile);
            response.put("fileSize", allowedLen);
            response.put("md5Sum", "");
            response.put("content", logStr.toString().substring(0, allowedLen));
        }
        else
        {
            response.put("error", ioErrStr);
        }

        mStaticEp.updateAttribute(StaticEndPoint.LOCAL_ID_CONFIG_LOG_FILES, response.toString());
        sendUpdatedAttributes(mStaticEp);
        return EAppService.STATUS_CODE.SUCCESS;
    }

    EAppService.STATUS_CODE CIInstallPackages(JSONArray params) throws JSONException {
        if (params.length() == 0) {
            return EAppService.STATUS_CODE.INVALID_PARAMETER;
        }
        String[] filePaths = new String[params.length()];
        for (int i = 0; i < params.length(); i++) {
            filePaths[i] = params.getString(i);
        }
        return CIInstallPackages(filePaths);
    }

    EAppService.STATUS_CODE CIInstallPackages(String[] filePaths) throws JSONException {
        return CIInstallPackages(filePaths, 0);
    }

    EAppService.STATUS_CODE CIInstallPackages(String[] filePaths, long timeoutMs) throws JSONException {
        if (filePaths.length == 0) {
            return EAppService.STATUS_CODE.SUCCESS;
        }

        JSONObject response;
        if (timeoutMs == 0) {
            response = mService.RmtMgmt_InstallAPKs(filePaths, false);
        }
        else {
            response = mService.RmtMgmt_InstallAPKs(filePaths, false, timeoutMs);
        }
        if (response == null) {
            Log.e(TAG, "Error installing packages");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }

        EAppService.STATUS_CODE r = EAppService.STATUS_CODE.SUCCESS;
        JSONArray fileStatusArr = response.getJSONArray(Common.PARAM_FILE_STATUS);
        for (int i = 0; i < fileStatusArr.length(); i++) {
            JSONObject fileStatus = fileStatusArr.getJSONObject(i);
            String filePath = fileStatus.getString(Common.PARAM_FILE_PATH);
            int status = fileStatus.getInt(Common.PARAM_STATUS);
            Log.e(TAG, "App:" + filePath + " status:" + status);
            if (status != 0) {
                r = EAppService.STATUS_CODE.API_RESPONSE_ERROR; /*TODO: add a more meaningful error */
            }
        }
        return r;
    }

    EAppService.STATUS_CODE CIProvision(String[] configs) throws JSONException {
        if (configs.length == 0) {
            return EAppService.STATUS_CODE.SUCCESS;
        }
        Log.d(TAG, "Provision: " + configs[0]);

        JSONObject response = mService.RmtMgmt_Provision(configs[0]);
        if (response == null) {
            Log.e(TAG, "Error provisioning config");
            return EAppService.STATUS_CODE.API_RESPONSE_ERROR;
        }

        EAppService.STATUS_CODE r = EAppService.STATUS_CODE.SUCCESS;
        JSONArray unzipStatusArr = response.getJSONArray("unzipStatus");
        for (int i = 0; i < unzipStatusArr.length(); i++) {
            JSONObject fileStatus = unzipStatusArr.getJSONObject(i);
            String filePath = fileStatus.getString(Common.PARAM_FILE_PATH);
            boolean status = fileStatus.getBoolean(Common.PARAM_STATUS);
            Log.e(TAG, "App:" + filePath + " status:" + status);
            if (!status) {
                r = EAppService.STATUS_CODE.API_RESPONSE_ERROR; /*TODO: add a more meaningful error */
            }
        }
        return r;
    }

    void handleCIEvent(int event, Object data) throws JSONException {
        switch (event) {
            case EAppAgent.CI_EVENT_NEW_OTA_IMAGE_AVAIL:
                ArrayList<String> apkList = new ArrayList<>();
                ArrayList<String> eAppApkList = new ArrayList<>();
                ArrayList<String> provApkList = new ArrayList<>();
                ArrayList<String> configList = new ArrayList<>();
                String manifest = (String)data;
                Log.d(TAG, "manifest is " + manifest);
                JSONObject json = new JSONObject(manifest);
                String version = json.getString("version");
                String oldVersion = mEappSettings.getOTAManifestVersion();

                if (oldVersion != null && oldVersion.equals(version)) {
                    Log.e(TAG, "We already have version:" + version);
                    return;
                }
                Log.e(TAG, "Old version:" + oldVersion + " New version:" + version + " are different. Updating...");

                Context context = EApp.getAppContext();
                PackageManager pm = context.getPackageManager();

                boolean updateApps = false;
                boolean updateEApp = false;
                boolean provisionApp = false;
                boolean updateRmtMgmtProv = false;
                EAppService.STATUS_CODE status;

                JSONArray pkgs = json.getJSONArray("packages");
                for (int i = 0; i < pkgs.length(); i++) {
                    JSONObject pkg = pkgs.getJSONObject(i);
                    String type = pkg.getString("type");
                    String url = pkg.getString("imageUrl");
                    String path = new File(url).getName();
                    path = mAgent.getOTAFileDir() + path;
                    Log.d(TAG, "Type:" + type + " path:" + path);
                    if (type.equals("APK")) {
                        PackageInfo info = pm.getPackageArchiveInfo(path, 0);
                        if (info.packageName.equals("com.flex.eapp") && !updateEApp) {
                            eAppApkList.add(path);
                            updateEApp = true;
                        }
                        else if (info.packageName.equals("com.intel.rmtmgmtprov.service") && !updateRmtMgmtProv) {
                            provApkList.add(path);
                            updateRmtMgmtProv = true;
                        }
                        else {
                            apkList.add(path);
                            updateApps = true;
                        }
                    } else if (type.equals("Connection Config")) {
                        configList.add(path);
                        provisionApp = true;
                    }

                }

                if (updateApps){
                    String[] apks = new String[apkList.size()];
                    status = CIInstallPackages(apkList.toArray(apks));
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        Log.e(TAG, "Install APKs failed");
                        return;
                    }
                }

                if (provisionApp) {
                    String[] configs = new String[configList.size()];
                    status = CIProvision(configList.toArray(configs));
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        //Log.e(TAG, "Provision failed at least one file..rebooting");
                    }
                }

                if (updateRmtMgmtProv) {
                    String[] provApks = new String[provApkList.size()];
                    provApkList.toArray(provApks);
                    if (provApks.length > 0) {
                        Log.d(TAG, "Installing " + provApks[0]);
                    }
                    status = CIInstallPackages(provApks, 1000);

                    RmtMgmt rmtMgmt = mService.getRmtMgmt();
                    int timeout = 120000;
                    int interval = 1000;
                    while (rmtMgmt.isReady() && timeout > 0) {
                       SystemClock.sleep(interval);
                        timeout -= interval;
                        Log.e(TAG, "Waiting for RmtMgmtProv service to disconnect:" + timeout +"ms left");
                    }

                    if (timeout <= 0) {
                        return;
                    }

                    status = rmtMgmt.start();
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        Log.e(TAG, "RmtMgmtProv service could not restart");
                        EAppControllerHandler handler = mService.getHandler();
                        Message newMsg = handler.obtainMessage(REQ_START_SERVICES);
                        handler.sendMessageDelayed(newMsg, REQ_START_SERVICE_DELAY);
                        Log.d(TAG, "Services not fully up - Retry in " + REQ_START_SERVICE_DELAY + "ms");
                        return;
                    }
                    else
                    {
                        Log.d(TAG, "RmtMgmtProv service back up");
                    }
                }

                /* Save settings here, before we update app */
                mEappSettings.setOTAManifestVersion(version);
                boolean rebootScheduled = false;

                if (updateEApp) {
                    /* IF EApp is installed, app will exit before we
                     * can reboot, or do anything else. Set the flag now,
                     * and clear it only if EApp install fails. In that
                     * case, the another call to CIReboot will take place
                     */
                    mEappSettings.setRebootAfterUpdateFlag();
                    mEappSettings.setCUIDBeforeUpdate(mEappSettings.get("CUSTOMER_UNIQUE_ID"));

                    String[] eAppApks = new String[eAppApkList.size()];
                    status = CIInstallPackages(eAppApkList.toArray(eAppApks));
                    if (status != EAppService.STATUS_CODE.SUCCESS) {
                        mEappSettings.clearRebootAfterUpdateFlag();
                        Log.e(TAG, "Install EApp APK failed");
                        return;
                    }
                }

                if (updateApps || provisionApp || updateEApp || updateRmtMgmtProv) {
                    Log.e(TAG, "Rebooting after OTA update...");
                    CIReboot();
                }
                break;

            case EAppAgent.CI_EVENT_IOTHUB_CONNECTION_DOWN:
                if (isConnected()) {
                    Log.d(TAG, "IOT hub down: Disconnecting agent");
                    setConnected(false);
                    mAgent.disconnect();
                }
                if (!isDecomissioned()) {
                    Log.d(TAG, "IOT hub down: Scheduling a connect");
                    mEAppJob.cancelJob(EAppJob.JOB_ID_SYNC);
                    mEAppJob.scheduleJob(EAppJob.JOB_ID_CONNECT);
                }
                break;
        }
    }

    void sendUpdatedAttributes(EndPoint ep) {
        //Log.d(TAG, "Sending updated attr for epId:" + ep.getEpId());
        HashMap<Integer, EndPoint.Attribute> attrs = ep.getUpdatedAttributes();
        for (Map.Entry<Integer, EndPoint.Attribute> entry : attrs.entrySet()) {
            EndPoint.Attribute attr = entry.getValue();
            Class<?> attrClass = attr.getValueClass();
            int localId = attr.getLocalId();

            if (attrClass.equals(String.class)) {
                String value = attr.getString();
                if (value != null) {
                    //Log.d(TAG, "   localId:" + localId + " desc:" + attr.getDesc() + " value:" + value);
                    mAgent.sendStringAttributeMsg(mStaticEp.getEpId(), localId, value);
                }
            }
            else if (attrClass.equals(Integer.class)) {
                Integer value = attr.getInt();
                if (value != null) {
                    //Log.d(TAG, "   localId:" + localId + " desc:" + attr.getDesc() + " value:" + value);
                    mAgent.sendIntAttributeMsg(mStaticEp.getEpId(), localId, value);
                }
            }
            else if (attrClass.equals(Long.class)) {
                Long value = attr.getLong();
                if (value != null) {
                    //Log.d(TAG, "   localId:" + localId + " desc:" + attr.getDesc() + " value:" + value);
                    mAgent.sendLongAttributeMsg(mStaticEp.getEpId(), localId, value);
                }
            }
            else if (attrClass.equals(Float.class)) {
                Float value = attr.getFloat();
                if (value != null) {
                    //Log.d(TAG, "   localId:" + localId + " desc:" + attr.getDesc() + " value:" + value);
                    mAgent.sendFloatAttributeMsg(mStaticEp.getEpId(), localId, value);
                }
            }
            else if (attrClass.equals(Boolean.class)) {
                Boolean value = attr.getBoolean();
                if (value != null) {
                    //Log.d(TAG, "   localId:" + localId + " desc:" + attr.getDesc() + " value:" + value);
                    mAgent.sendBooleanAttributeMsg(mStaticEp.getEpId(), localId, value);
                }
            }
            else {
                Log.e(TAG, "Unable to find sender for attribute localId:" + localId);
            }
            attr.setUpdated(false);
        }
    }

    String getDeviceAttribute(int ep, String key) {
        if (ep != StaticEndPoint.EP_ID) {
            return null;
        }
        if (key.equals("DEVICE_ID")) {
            return mStaticEp.mDeviceInfo.mIdentifiers.mIMEINumber;
        }
        else if (key.equals("DEVICE_SN")) {
            return mStaticEp.mDeviceInfo.mIdentifiers.mSerialNumber;
        }
        return null;
    }

    EAppService.STATUS_CODE start() {
        Context context = EApp.getAppContext();

        mEappSettings = new EAppSettings();
        mStaticEp = new StaticEndPoint();
        mAgent = new EAppAgent(this);
        mBtDb = new BtDeviceDb(context);
        mEAppJob = new EAppJob();
        mBtDb.open();

        mEappSettings.updateFromFile();

        if (mEappSettings.getRebootAfterUpdateFlag()) {
            mRebootAfterUpdate = true;
        }

        String oldCuid = mEappSettings.getCUIDBeforeUpdate();
        if (oldCuid != null && !oldCuid.isEmpty()) {
            String newCuid = mEappSettings.get("CUSTOMER_UNIQUE_ID");
            if (newCuid != null && !newCuid.isEmpty()) {
                if (!oldCuid.equals(newCuid)) {
                /* We have a new CUID, remove old nvm.bin */
                    Log.d(TAG, "Switching CUID from " + oldCuid + " to " + newCuid);
                    mAgent.clearNVMFile();
                }
                mEappSettings.clearCUIDBeforeUpdate();
            }
        }

        setReady(true);
        return EAppService.STATUS_CODE.SUCCESS;
    }

    void stop() {
        EAppJob.cancelAllJobs();
        Log.e(TAG, "Controller stopped");
    }

    EAppService.STATUS_CODE bindToService() {
        Context context = EApp.getAppContext();

        Intent intent = new Intent(context, EAppService.class);
        intent.putExtra("STOP_SERVICE", true);
        if (!context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE)) {
            Log.e(TAG, "Unable to bind to EAppService");
            return EAppService.STATUS_CODE.BIND_FAILED;
        }
        return EAppService.STATUS_CODE.SUCCESS;
    }

    void unbindFromService() {
        Context context = EApp.getAppContext();
        context.unbindService(mConnection);
    }

    synchronized boolean isReady() {
        return mReady;
    }

    synchronized void setReady(boolean ready) {
        mReady = ready;
    }

    synchronized boolean isConnected() {
        return mConnected;
    }

    synchronized void setConnected(boolean connected) {
        mConnected = connected;
    }

    synchronized boolean isDecomissioned() {
        return mDecomissioned;
    }

    synchronized void setDecommisioned(boolean decom) {
        mDecomissioned = decom;
    }
}
