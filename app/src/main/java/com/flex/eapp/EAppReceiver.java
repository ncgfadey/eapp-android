/*---------------------------------------------------------
 *
 * Flex
 *
 * Copyright 2017
 *
 * This file may not be copied, modified, or duplicated
 * without written permission from Flex
 * in the terms outlined in the license provide to the
 * end user/users of this file.
 *
 ---------------------------------------------------------*/

/*
 * Created by Femi.Adeyemi@flex.com
 */

package com.flex.eapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class EAppReceiver extends BroadcastReceiver {
    private static final String TAG = "EAppReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        Intent newIntent = new Intent(context, EAppActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d(TAG, "Boot completed - starting EApp...");
            newIntent.putExtra(EAppActivity.EXTRA_START_REASON,
                    EAppActivity.START_REASON_BOOT_COMPLETED);
            context.startActivity(newIntent);

        }
        else if (action.equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {
            Log.d(TAG, "EApp was updated");
            newIntent.putExtra(EAppActivity.EXTRA_START_REASON,
                    EAppActivity.START_REASON_EAPP_UPDATED);

            context.startActivity(newIntent);
        }
        else  {
            Log.e(TAG, "Unhandled broadcast:" + action);
        }
    }
}
