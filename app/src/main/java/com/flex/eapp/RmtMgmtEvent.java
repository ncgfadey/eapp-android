package com.flex.eapp;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Alex.Molina@flex.com
 */
abstract class RmtMgmtEvent {
    public static final String TAG = "eAPP_RmtMgmtEvent";
    /*
     * JSON keys common to all RemoteMgmtProv events
     */
    static final String EVENT_TYPE_KEY = "eventType";
    static final String VERSION_KEY = "version";

    final protected String mEventTypeStr;
    final protected JSONObject mEventObj;
    final protected EventTypes mEventType;

    RmtMgmtEvent(final JSONObject eventObj)
            throws JSONException {
        try {
            mEventTypeStr = eventObj.getString(EVENT_TYPE_KEY);
            mEventType = EventTypes.getEventType(eventObj);
        } catch (JSONException jerr) {
            Log.e(TAG, EVENT_TYPE_KEY + " not found");
            throw jerr;
        }
        mEventObj = eventObj;
    }

    public EventTypes getEventType() {
        return mEventType;
    }

    public String toString() {
        return mEventTypeStr;
    }


    static RmtMgmtEvent getEventObject(JSONObject jObj)
            throws JSONException {

        final EventTypes eventType = EventTypes.getEventType(jObj);

        RmtMgmtEvent eventObj = null;

        switch (eventType) {
            case BT_PAIRED:
                eventObj = new BtPairedEvent(jObj);
                break;

            case BT_UNPAIRED:
                eventObj = new BtUnpairedEvent(jObj);
                break;

            case CALL_HOME:
                eventObj = new CallHomeEvent(jObj);
                break;

            case DECOMMISIONED:
                eventObj = new DecommissionEvent(jObj);
                break;

            default:
                /*
                 *  Shouldn't get here
                 */
                Log.e(TAG, "Invalid RmtMgmtEvent enum");
                break;
        }  // end switch

        if (eventObj == null) {
            throw new NullPointerException(TAG + " failed to create Event object");
        }
        return eventObj;
    }

    static enum EventTypes {
        BT_PAIRED("BT Paired"),
        BT_UNPAIRED("BT Unpaired"),
        CALL_HOME("Call Home"),
        DECOMMISIONED("Decommissioned");

        private static final Map<String, EventTypes> mEventKeyMap;

        static {
            Map<String, EventTypes> keyMap = new HashMap<>(EventTypes.values().length);
            keyMap.put(BT_PAIRED.toString(), BT_PAIRED);
            keyMap.put(BT_UNPAIRED.toString(), BT_UNPAIRED);
            keyMap.put(CALL_HOME.toString(), CALL_HOME);
            keyMap.put(DECOMMISIONED.toString(), DECOMMISIONED);
            mEventKeyMap = Collections.unmodifiableMap(keyMap);
        }

        private final String mEventType;

        EventTypes(final String eventType) {
            mEventType = eventType;
        }

        static public EventTypes getEventType(JSONObject jObj)
                throws JSONException {
            final String eventTypeStr = jObj.getString(EVENT_TYPE_KEY);
            final EventTypes eventType = EventTypes.getEventType(eventTypeStr);
            if (eventType == null) {
                throw new JSONException("Unknown eventType: " + eventTypeStr);
            }
            return eventType;
        }

        static public EventTypes getEventType(String keyName) {
            return mEventKeyMap.get(keyName);
        }

        /*
         * event name
         */
        @Override
        public String toString() {
            return mEventType;
        }

    }
}
