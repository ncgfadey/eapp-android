#ifndef OTA_EMW_OTA_H_
#define OTA_EMW_OTA_H_

extern "C"
{
   #include <stdio.h>
   #include <string.h>
   #include <stdlib.h>
   #include <unistd.h>
   
   #include <flx-ci-agent.h>
   #include <flx-config.h>
   
   #include <flx-util-ota.h>
   #include <flx-util-json.h>
   #include <flx-util-sha256.h>
   #include <flx-util-base64.h>
   #include <flx-util-rsa.h>
   #include <flx-log.h>
   #include <flx-security-profile.h>
   
   #include <curl/curl.h>
   #include <openssl/sha.h>
}


#pragma GCC diagnostic ignored "-Wwrite-strings" // removes C++ vs C string warnings


int ota_get_update_verify( FLX_CI_Event_t *event, char* update_url, char* update_hash, char* update_version, char *ota_dir);
size_t ota_write_data(void *ptr, size_t size, size_t nmemb, void *stream);
int ota_download_image(char *image_url, char* hash, char *ota_dir);
int ota_validate_image(char* hash, char* filename);
int ota_boot_image(char* image_file);
int ota_update_boot_image_symlinks();
int ota_reboot();
int ota_get_current_image(char** image_file);
int ota_get_good_image(char** image_file);
int ota_get_factory_image(char** image_file);


#endif /* OTA_EMW_OTA_H_ */
