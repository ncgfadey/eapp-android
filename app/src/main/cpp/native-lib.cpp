#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_flex_eapp_EAppActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Eapp 0.1";
    return env->NewStringUTF(hello.c_str());
}
