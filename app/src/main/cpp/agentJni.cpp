#include <jni.h>
#include <string>
#include <flx-ci-agent.h>
#include "agent.h"

#ifdef __cplusplus
extern "C" {
#endif

agent *g_agent;
FLX_CI_Device_Info_t g_deviceInfo;

nativeClientEnv cEnv;

char g_otaFileDir[128];
char g_nvmFilename[128];
char g_nvmAttrFilename[128];
char g_agentDBFilename[128];
char g_certFilename[128];

static bool g_agentIsInit = false;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {
    cEnv.vm = vm;

    JNIEnv* env = NULL;
    if (vm->GetEnv((void **)&env, JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }
    jclass cls = env->FindClass("com/flex/eapp/EAppAgent");
    if (!cls) {
        return -1;
    }
    cEnv.cls = (jclass)env->NewGlobalRef(cls);
    FLX_PAL_LOG_NOISE("Agent loaded");
    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved) {
    cEnv.env->DeleteGlobalRef(cEnv.cls);
    FLX_PAL_LOG_NOISE("Agent unloaded");
}


static const char* agentJni_getDeviceInfoDetail(JNIEnv *env, jobject obj,
                                         jmethodID methodId, const char *key);
static int agentJni_getDeviceInfo(JNIEnv *env, jobject obj,
                                          jmethodID methodId, FLX_CI_Device_Info_t *deviceInfo);

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniInit
        (JNIEnv *env, jobject obj)
{
    if (g_agentIsInit) {
        return FLX_CI_OK;
    }

    jclass eAppCls = env->GetObjectClass(obj);

    jmethodID getOTAFileDirId = env->GetMethodID(eAppCls, "getOTAFileDir",
                                                 "()Ljava/lang/String;");
    jstring otaFileDir = (jstring)env->CallObjectMethod(obj, getOTAFileDirId);
    if (otaFileDir) {
        std::string otaFileDirStr = std::string(env->GetStringUTFChars(otaFileDir, 0));
        strncpy(g_otaFileDir, otaFileDirStr.c_str(), sizeof(g_otaFileDir));
    }

    jmethodID getNVMFilenameId = env->GetMethodID(eAppCls, "getNVMFilename",
                                                 "()Ljava/lang/String;");
    jstring nvmFilename = (jstring)env->CallObjectMethod(obj, getNVMFilenameId);
    if (nvmFilename) {
        std::string nvmFilenameStr = std::string(env->GetStringUTFChars(nvmFilename, 0));
        strncpy(g_nvmFilename, nvmFilenameStr.c_str(), sizeof(g_nvmFilename));
    }

    jmethodID getNVMAttrFilenameId = env->GetMethodID(eAppCls, "getNVMAttrFilename",
                                                 "()Ljava/lang/String;");
    jstring nvmAttrFilename = (jstring)env->CallObjectMethod(obj, getNVMAttrFilenameId);
    if (nvmAttrFilename) {
        std::string nvmAttrFilenameStr = std::string(env->GetStringUTFChars(nvmAttrFilename, 0));
        strncpy(g_nvmAttrFilename, nvmAttrFilenameStr.c_str(), sizeof(g_nvmAttrFilename));
    }

    jmethodID getAgentDBFilenameId = env->GetMethodID(eAppCls, "getAgentDBFilename",
                                                 "()Ljava/lang/String;");
    jstring agentDBFilename = (jstring)env->CallObjectMethod(obj, getAgentDBFilenameId);
    if (agentDBFilename) {
        std::string agentDBFilenameStr = std::string(env->GetStringUTFChars(agentDBFilename, 0));
        strncpy(g_agentDBFilename, agentDBFilenameStr.c_str(), sizeof(g_agentDBFilename));
    }

     jmethodID getCertFilenameId = env->GetMethodID(eAppCls, "getCertFilename",
                                                   "()Ljava/lang/String;");
    jstring certFilename = (jstring)env->CallObjectMethod(obj, getCertFilenameId);
    if (certFilename) {
        std::string certFilenameStr = std::string(env->GetStringUTFChars(certFilename, 0));
        strncpy(g_certFilename, certFilenameStr.c_str(), sizeof(g_certFilename));
    }

    jmethodID getDeviceInfoId = env->GetMethodID(eAppCls, "getDeviceInfo",
                                    "(Ljava/lang/String;)Ljava/lang/String;");
    if (!getDeviceInfoId) {
        return -1;
    }
    agentJni_getDeviceInfo(env, obj, getDeviceInfoId, &g_deviceInfo);
    g_agent = new agent(&cEnv);
    int ret = g_agent->init(&g_deviceInfo, (char *)g_certFilename);
    if (!ret) {
        g_agentIsInit = true;
    }
    agent::setJvmReady(true);
    return ret;
}

JNIEXPORT jstring JNICALL Java_com_flex_eapp_EAppAgent_jniGetAgentLibVersion
        (JNIEnv *env, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        return  env->NewStringUTF("Agent Unavailable");
    }
    std::string version = g_agent->getAgentLibVersion();
    return env->NewStringUTF(version.c_str());
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniGetActivationStatus
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("GetActivationStatus: Agent not ready");
        return  0;
    }
    return g_agent->getActivationStatus();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniActivate
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("Activate: Agent not ready");
        return  0;
    }
    return g_agent->activate();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSync
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("Sync: Agent not ready");
        return  0;
    }
    return g_agent->sync();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniDeactivate
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("Deactivate: Agent not ready");
        return  0;
    }
    return g_agent->deactivate();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniConnect
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("Connect: Agent not ready");
        return  0;
    }
    return g_agent->connect();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniDisconnect
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("Disconnect: Agent not ready");
        return  0;
    }
    return g_agent->disconnect();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniClearActivationData
        (JNIEnv *, jobject)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("ClearActivationData: Agent not ready");
        return 0;
    }

    return g_agent->clearActivationData();
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSendStringAttributeMsg
        (JNIEnv *env, jobject, jint epId, jint localId, jstring value)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("SendAttrMsg: Agent not ready");
        return  0;
    }
    const char *valueStr = env->GetStringUTFChars(value, 0);

    FLX_CI_String_t str = {(uint8_t *)valueStr, strlen(valueStr)};

    FLX_CI_DataValue_t dataValue;
    dataValue.stringData = str;

    FLX_CI_DataElement_t attr = {dataValue,
                                 FLX_CI_DATA_TYPE_STRING};
    return g_agent->sendAttributeMsg(epId, localId, &attr);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSendIntAttributeMsg
        (JNIEnv *env, jobject, jint epId, jint localId, jint value)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("SendIntAttrMsg: Agent not ready");
        return  0;
    }

    FLX_CI_DataValue_t dataValue;
    dataValue.int32Value = value;

    FLX_CI_DataElement_t attr = {dataValue,
                                 FLX_CI_DATA_TYPE_INT32};
    return g_agent->sendAttributeMsg(epId, localId, &attr);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSendLongAttributeMsg
        (JNIEnv *env, jobject, jint epId, jint localId, jlong value)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("SendLongAttrMsg: Agent not ready");
        return  0;
    }

    FLX_CI_DataValue_t dataValue;
    dataValue.bigintValue = value;

    FLX_CI_DataElement_t attr = {dataValue,
                                 FLX_CI_DATA_TYPE_BIGINT};
    return g_agent->sendAttributeMsg(epId, localId, &attr);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSendFloatAttributeMsg
        (JNIEnv *env, jobject, jint epId, jint localId, jfloat value)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("SendFloatAttrMsg: Agent not ready");
        return  0;
    }

    FLX_CI_DataValue_t dataValue;
    dataValue.floatValue = value;

    FLX_CI_DataElement_t attr = {dataValue,
                                 FLX_CI_DATA_TYPE_FLOAT};
    return g_agent->sendAttributeMsg(epId, localId, &attr);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniSendBooleanAttributeMsg
        (JNIEnv *env, jobject, jint epId, jint localId, jboolean value)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("SendBooleanAttrMsg: Agent not ready");
        return  0;
    }

    FLX_CI_DataValue_t dataValue;
    dataValue.boolValue = value;

    FLX_CI_DataElement_t attr = {dataValue,
                                 FLX_CI_DATA_TYPE_BOOL};
    return g_agent->sendAttributeMsg(epId, localId, &attr);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniAddDynamicEndpoint
        (JNIEnv *env, jobject jobj,
         jint epId, jstring mfgId, jstring userField, jstring name)
{
    if (!g_agent || !g_agent->jvmIsReady()) {
        FLX_PAL_LOG_NOISE("AddDynEp: Agent not ready");
        return  0;
    }

    FLX_CI_Multi_Ep_t multi_ep = {};
    const char *mfgIdStr = env->GetStringUTFChars(mfgId, 0);
    const char *userFieldStr = env->GetStringUTFChars(userField, 0);
    const char *nameStr = env->GetStringUTFChars(name, 0);

    multi_ep.LocalEpId = epId;
    strncpy(multi_ep.ManufacturerId, mfgIdStr, sizeof(multi_ep.ManufacturerId));
    strncpy(multi_ep.UserField, userFieldStr, sizeof(multi_ep.UserField));
    strncpy(multi_ep.Name, nameStr, sizeof(multi_ep.Name));

    return g_agent->addDynamicEndpoint(&multi_ep,1);
}


JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniDeleteDynamicEndpoint
        (JNIEnv *env, jobject, jint epId) {
    if (!g_agent) {
        FLX_PAL_LOG_NOISE("DelDynEp: Agent not ready");
        return 0;
    }

    return g_agent->deleteDynamicEndpoint(epId);
}

JNIEXPORT jint JNICALL Java_com_flex_eapp_EAppAgent_jniStop
        (JNIEnv *env, jobject obj)
{
    if (!g_agent) {
        FLX_PAL_LOG_NOISE("Stop: Agent not ready");
        return  0;
    }
    agent::setJvmReady(false);
    delete g_agent;
    g_agent = NULL;
    return 0;
}

static const char* agentJni_getDeviceInfoDetail(JNIEnv *env, jobject obj,
                                                jmethodID methodId, const char *key)
{
    jstring value = (jstring)env->CallObjectMethod(obj, methodId,
                                                   env->NewStringUTF(key));

    if (!value) {
        return NULL;
    }
    std::string valueStr = std::string(env->GetStringUTFChars(value, 0));
    return valueStr.c_str();
}

static int agentJni_getDeviceInfo(JNIEnv *env, jobject obj,
                                  jmethodID methodId, FLX_CI_Device_Info_t *deviceInfo)
{

    const char *valueStr;

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_MAC");
    strncpy(deviceInfo->MAC, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_OS");
    strncpy(deviceInfo->OS, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_OS_VERSION");
    strncpy(deviceInfo->OSVersion, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_API_VERSION");
    strncpy(deviceInfo->ApiVersion, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_PLATFORM_ARCH");
    strncpy(deviceInfo->PlatformArch, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_AGENT_TYPE");
    strncpy(deviceInfo->AgentType, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_RMA");
    strncpy(deviceInfo->RMA, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_RMA_DATE");
    strncpy(deviceInfo->RmaDate, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_HW_VERSION");
    strncpy(deviceInfo->HwVersion, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_BATCH");
    strncpy(deviceInfo->Batch, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_BUILD_DATE");
    strncpy(deviceInfo->Builddate, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_SN");
    strncpy(deviceInfo->SN, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_SW_VERSION");
    strncpy(deviceInfo->SwVersion, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_SW_NAME");
    strncpy(deviceInfo->SwName, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_HW_NAME");
    strncpy(deviceInfo->HwName, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_STATUS");
    strncpy(deviceInfo->Status, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "DEVICE_ID");
    strncpy(deviceInfo->DeviceId, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    valueStr = agentJni_getDeviceInfoDetail(env, obj, methodId, "CUSTOMER_UNIQUE_ID");
    strncpy(deviceInfo->CustomerUniqueId, valueStr, FLX_CI_DEVICE_INFO_FIELD_MAX_LEN);

    return 0;
}


char *EApp_getOTAFileDir()
{
    return g_otaFileDir;
}
const char *EApp_getNVMFilename()
{
    return g_nvmFilename;
}
const char *EApp_getNVMAttrFilename()
{
    return g_nvmAttrFilename;
}
char *EApp_getAgentDBFilename()
{
    return g_agentDBFilename;
}


#ifdef __cplusplus
}
#endif


