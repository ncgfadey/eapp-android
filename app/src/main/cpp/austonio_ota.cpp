#include "austonio_ota.hpp"
#include "agent.h"



//FIXME: put in config file

unsigned char img_buf[OTA_IMG_BUF_SIZE] = {0};
unsigned char hash[65] = {0};




// OTA helper functions
int ota_get_update_verify( FLX_CI_Event_t *event, char* update_url, char* update_hash, char* update_version, char *ota_dir )
{

    int ota_manifest_len = strlen(event->data.ota_manifest);
    char *manifest_buf = new char[ota_manifest_len+1];
    memset(manifest_buf, 0, ota_manifest_len+1);
    //char manifest_buf[FLX_CI_OTA_MANIFEST_LEN]={0};

   FLX_PAL_LOG_NOISE("ota module got manifest %s", event->data.ota_manifest);

   strncpy(manifest_buf, event->data.ota_manifest, ota_manifest_len);

   FLX_PAL_LOG_NOISE("Copied manifest");

   flx_util_json_token_t* tokens;
   flx_util_json_parser_t jp;
   int numTok = 0;
   int currToken = 0;
   int ret = 0;
   

   /* allocate tokens */
   tokens = new flx_util_json_token_t[FLX_CI_MAX_TOKENS];
   if( NULL == tokens ) {
       FLX_PAL_LOG_NOISE("Error allocating tokens");
       return FLX_CI_OUT_OF_MEMORY;
   }
    //FLX_PAL_LOG_NOISE("Allocated tokens");


   flx_util_json_parser_init(&jp);
   //FLX_PAL_LOG_NOISE("Initialized parser");
   
   numTok = flx_util_json_parse(&jp, manifest_buf, strlen(manifest_buf), tokens, FLX_CI_MAX_TOKENS);

   if( numTok < 0 )
   {
      FLX_PAL_LOG_NOISE("Token error %d", numTok);
      /* free resources, error out */
      free(tokens);
      delete [] manifest_buf;
      if( numTok == -1 )
      {
         return FLX_CI_JSON_OUT_OF_TOKENS;
      }
      return FLX_CI_JSON_PARSE_ERROR;
   }

    //currToken = flx_util_json_search(tokens, numTok, manifest_buf, "SWversion");
    currToken = flx_util_json_search(tokens, numTok, manifest_buf, "version");
    //FLX_PAL_LOG_NOISE("Searched version");

    /* copy the non-null terminated string out of a json token */
    flx_util_json_strncpy( update_version, &tokens[currToken + 1], manifest_buf, tokens[currToken + 1].end - tokens[currToken + 1].start + 1);
    //FLX_PAL_LOG_NOISE("Found version:%s",update_version);

    //currToken = flx_util_json_search(tokens, numTok, manifest_buf, "ImageHash");
    currToken = flx_util_json_search(tokens, numTok, manifest_buf, "imageHash");
    //FLX_PAL_LOG_NOISE("Searched hash");

    /* copy the non-null terminated string out of a json token */
    flx_util_json_strncpy( update_hash, &tokens[currToken + 1], manifest_buf, tokens[currToken + 1].end - tokens[currToken + 1].start + 1);
    //FLX_PAL_LOG_NOISE("1st parsed Hash:%s",update_hash);

    //currToken = flx_util_json_search(tokens, numTok, manifest_buf, "UpdateUrl");
    currToken = flx_util_json_search(tokens, numTok, manifest_buf, "imageUrl");
    //FLX_PAL_LOG_NOISE("Searched URL");

    /* copy the non-null terminated string out of a json token */
    flx_util_json_strncpy( update_url, &tokens[currToken + 1], manifest_buf, tokens[currToken + 1].end - tokens[currToken + 1].start + 1);
    //FLX_PAL_LOG_NOISE("1st Parsed URL : %s",update_url );

    ret = ota_download_image( update_url,  update_hash, ota_dir);

    if ( ret == FLX_CI_OK )
    {
        FLX_PAL_LOG_NOISE("Package Download + hash validation Success!");
    }
    else
    {
        return ret;
        FLX_PAL_LOG_NOISE("Download failed: %d", ret);
    }
    // Check if there is anymore package to download
    while(currToken != -1) {

        //currToken = flx_util_json_search(tokens, numTok, manifest_buf, "ImageHash");
        currToken = flx_util_json_search_resume(tokens, numTok, manifest_buf, "imageHash",currToken + 1);
        //FLX_PAL_LOG_NOISE("currToken:%d", currToken);
        //FLX_PAL_LOG_NOISE("Searched hash");

        if (currToken != -1) {
            /* copy the non-null terminated string out of a json token */
            flx_util_json_strncpy(update_hash, &tokens[currToken + 1], manifest_buf, tokens[currToken + 1].end - tokens[currToken + 1].start + 1);
            //FLX_PAL_LOG_NOISE("parsed Hash:%s", update_hash);

            currToken = flx_util_json_search_resume(tokens, numTok, manifest_buf, "imageUrl", currToken + 1);
            /* copy the non-null terminated string out of a json token */
            flx_util_json_strncpy(update_url, &tokens[currToken + 1], manifest_buf, tokens[currToken + 1].end - tokens[currToken + 1].start + 1);
            //FLX_PAL_LOG_NOISE("parsed URL : %s",update_url );
            //FLX_PAL_LOG_NOISE("currToken:%d", currToken);

            ret = ota_download_image( update_url,  update_hash, ota_dir);
            if ( ret == FLX_CI_OK )
            {
                FLX_PAL_LOG_NOISE("Package Download + hash validation Success!");
            }
            else
            {
                return ret;
                FLX_PAL_LOG_NOISE("Download failed: %d", ret);
            }
        }
    }



   /* free resources, return */
   free(tokens);
   delete [] manifest_buf;
   //FLX_PAL_LOG_NOISE("Freed tokens");
   return 0;
}   

size_t ota_write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
   size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
   return written;
}

int ota_download_image(char* image_url, char* image_hash, char *ota_dir)
{
   int status = 0;
   CURL *curl_handle;
   CURLcode ret = CURLE_OK;
   FILE *imgFile;
   ret = curl_global_init(CURL_GLOBAL_ALL);
   //FLX_PAL_LOG_NOISE("curl_global_init:%d", ret);
   
   /* init the curl session */ 
   curl_handle = curl_easy_init();
   //FLX_PAL_LOG_NOISE("curl_easy_init");
   
   /* set URL to get here */ 
   ret = curl_easy_setopt(curl_handle, CURLOPT_URL, image_url);
   //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_URL, image_url):%d", ret);
   
   /* Switch on full protocol/debug output while testing */ 
   ret = curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
   //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L}:%d", ret);

   /* disable progress meter, set to 0L to enable and disable debug output */ 
   ret = curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
   //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L):%d", ret);

   /* send all data to this function  */ 
   ret = curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, ota_write_data);
   //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, ota_write_data):%d", ret);

    //Need this to work. Any risks?
    ret = curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, false);
    //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, ota_write_data):%d", ret);

    char * filename;
    char file_path[256];
    filename = basename(image_url);
    FLX_PAL_LOG_NOISE("filename:%s", filename);
    status = snprintf(file_path,256,"%s%s", ota_dir, filename);
    if(status<0)
    {
        return -1;
    }
    FLX_PAL_LOG_NOISE("file_path:%s", file_path);

    /* open the file */
    imgFile = fopen(file_path, "wb");
    if(imgFile) {
      //FLX_PAL_LOG_NOISE("imgFile = fopen()", ret);
   
      /* write the image body to this file handle */ 
      ret = curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, imgFile);
      //FLX_PAL_LOG_NOISE("curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, imgFile):%d", ret);
      
      /* get it! */ 
       ret = curl_easy_perform(curl_handle);
       //FLX_PAL_LOG_NOISE("curl_easy_perform(curl_handle):%d", ret);

      /* close the header file */ 
      fclose(imgFile);
   }
   else
   {
      ret = CURLE_REMOTE_FILE_NOT_FOUND;
	  FLX_PAL_LOG_NOISE("CURLE_REMOTE_FILE_NOT_FOUND");

   }
   /* cleanup curl stuff */ 
   curl_easy_cleanup(curl_handle);
   
   if( ret != CURLE_OK )
   {
      return ret;
   }   
   
   // Now validates the hash
   
   status = ota_validate_image(image_hash, file_path);
   if( status != 0)
   {
     return status; 
   }
   else return FLX_CI_OK;
};

int ota_validate_image(char* hash_cloud, char* filename)
{ 
   //FLX_PAL_LOG_NOISE("Validating SHA256 hash of the image");
   
   long int bytesRead = 0;
   char hash_string[65]={0};
   
   SHA256_CTX sha256;
   SHA256_Init(&sha256);
   
   // puts the file in a buffer for SHA256 operations
   FILE *f = fopen(filename, "rb");
   if(!f) return -2;
   
   bytesRead = fread(img_buf, 1, OTA_IMG_BUF_SIZE, f);
    //FLX_PAL_LOG_NOISE("1st Bytes read: %d\n", bytesRead);
   while( bytesRead !=0)
   {
       //FLX_PAL_LOG_NOISE("Bytes read: %d\n", bytesRead);
      
      SHA256_Update(&sha256, img_buf, bytesRead);
      bytesRead = fread(img_buf, 1, OTA_IMG_BUF_SIZE, f);
   }   
   fclose(f);
   
   SHA256_Final(hash, &sha256);

   // convert to string
   for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
   {
      sprintf(hash_string + (i * 2), "%02X", hash[i]);
   }
   
   hash_string[64] = 0;

    FLX_PAL_LOG_NOISE("Hash calculated for downloaded file: %s\n", hash_string);
   
   if(!strncmp(hash_string, hash_cloud, 64))
   {
      //printf("hashes are identical!"\n);
      return 0;
   }
   else
   {
       FLX_PAL_LOG_NOISE("hashes are different! Now deleting the file...\n");
      
      if ( remove(filename)!=0 ) return -2;
      return -3;
   }
};

int ota_boot_image(char* image_file)
{
    return 0;
   
};

int ota_update_boot_image_symlinks()
{
   return 0;
};

int ota_reboot()
{
   return 0;
};

int ota_get_current_image(char** image_file)
{
    return 0;
};

int ota_get_good_image(char** image_file)
{
    return 0;
};

int ota_get_factory_image(char** image_file)
{
    return 0;
};


