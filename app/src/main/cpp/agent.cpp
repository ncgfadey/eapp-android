extern "C" {
#include <flx-security-profile.h>
}
#include "agent.h"
#include "austonio_ota.hpp"


using namespace std;


extern nativeClientEnv cEnv;
const char *agent::TAG = "AgentJNI";
//nativeClientEnv agent::cenv = &cenv;
pthread_mutex_t agent::mtx = PTHREAD_MUTEX_INITIALIZER;
bool agent::jvmReady = false;

flx_pal_security_profile_entry_t sec_profile[FLX_PAL_SECURITY_PROFILE_LENGTH] = { {"ECDHE-RSA-AES256-SHA384","/sdcard/ca-bundle.crt"},{"",""},{"",""},{"",""} };

int agent::init(FLX_CI_Device_Info_t *deviceInfo, char *cert)
{
    sec_profile[0].cert_file = cert;
    flx_pal_security_profile_set(sec_profile);
    int ret = FLX_CI_Init(deviceInfo, agent::eventCallback);
    return ret;
}

std::string agent::getAgentLibVersion()
{
    std::string version = FLX_CI_GetAgentLibVersion();
    return version;
}

int agent::getActivationStatus()
{
    return FLX_CI_GetActivationStatus();
}

int agent::clearActivationData() {

    return FLX_CI_ClearActivationData();
}

int agent::activate()
{
    if (FLX_CI_GetActivationStatus() == FLX_CI_OK) {
        /* TODO: Do we want to handle such logic here, or defer to eAppController */
        //FLX_PAL_LOG_NOISE("Already activated...");
        //return 0;
    }
    return FLX_CI_Activate();
}

int agent::deactivate()
{
    // use conn_handle == 0,
    // seems to have problems re-using a connection handle
    return FLX_CI_Deactivate(0);
}

int agent::sync()
{
    return FLX_CI_Sync();
}

int agent::connect()
{
    return FLX_CI_Connect(messageArrived, &conn_handle);
}

int agent::disconnect()
{
    return FLX_CI_Disconnect(conn_handle);
}

int agent::sendAttributeMsg(uint32_t epId, uint32_t attrId,
                            FLX_CI_DataElement_t *attr_data_elem)
{
    return FLX_CI_SendAttributeMessage(conn_handle,
                                       epId,
                                       attrId,
                                       attr_data_elem);
}

int agent::addDynamicEndpoint(FLX_CI_Multi_Ep_t *multiEp, int numOfEps){
    return FLX_CI_AddDynamicEndpoint(multiEp, numOfEps);
}

int agent::deleteDynamicEndpoint(uint32_t  epId) {
    return FLX_CI_DeleteDynamicEndpoint(epId);
}

void agent::eventCallback(FLX_CI_Event_t *event)
{
    char update_url[FLX_CI_OTA_MANIFEST_URL_LEN] = {0};
    char imgHash[65] = {0};
    char update_version[32] = {0};
    int ret = -1;

    if (!jvmIsReady()) {
        FLX_PAL_LOG_NOISE("JVM is not ready for event %d", event->type);
        return;
    }

    switch (event->type) {
        case FLX_CI_NEW_OTA_IMAGE_AVAIL: 
		{
            FLX_PAL_LOG_NOISE("Downloaded and RSA decrypted manifest from URL received at sync%s",
                              event->data.ota_manifest);
            // get package url
            FLX_PAL_LOG_NOISE("Start parsing Manifest...");
            ret = ota_get_update_verify(event, update_url, imgHash, update_version, EApp_getOTAFileDir());
            
            if (ret) {
                FLX_PAL_LOG_NOISE("OTA update failed");
            }
        }	
        break;
        case FLX_CI_IOTHUB_CONNECTION_DOWN:
            ret = 0;
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_IOTHUB_CONNECTION_DOWN received");
            break;
        case FLX_CI_IOTHUB_CONNECTION_UP:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_IOTHUB_CONNECTION_UP received");
            break;
        case FLX_CI_WIFI_CONNECTION_DOWN:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_WIFI_CONNECTION_DOWN received");
            break;
        case FLX_CI_WIFI_CONNECTION_UP:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_WIFI_CONNECTION_UP received");
            break;
        case FLX_CI_TIMESTAMP_AVAILABLE:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_TIMESTAMP_AVAILABLE received");
            break;
        case FLX_CI_EVT_MQTT_SEND_SUCCESS:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_EVT_MQTT_SEND_SUCCESS received");
            break;
        case FLX_CI_INVALID_OTA_IMAGE:
            FLX_PAL_LOG_NOISE("FLX CI event FLX_CI_INVALID_OTA_IMAGE received");
            break;
        case FLX_CI_EVT_NUM_EVENTS:
            FLX_PAL_LOG_NOISE("FLX CI invalid event FLX_CI_EVT_NUM_EVENTS received");
            break;
        default:
            FLX_PAL_LOG_NOISE("FLX CI unhandled event %d received", event->type);
            break;
    }

    if (ret != 0) {
        return;
    }
    bool manualAttach = false;
    if (cEnv.vm->GetEnv((void **)&cEnv.env, JNI_VERSION_1_6) == JNI_EDETACHED) {
        manualAttach = true;
        if (cEnv.vm->AttachCurrentThread(&cEnv.env, NULL) != JNI_OK) {
            FLX_PAL_LOG_NOISE("Unable to attach to eApp service");
            return;
        }
    }

    cEnv.cbID = cEnv.env->GetStaticMethodID(cEnv.cls,"processAgentEvent",
                                            "(ILjava/lang/String;)V");
    if (!cEnv.cbID) {
        FLX_PAL_LOG_NOISE("Unable to send message to eApp service");
        if (manualAttach) {
            cEnv.vm->DetachCurrentThread();
        }
        return;
    }

    /* Repeated switch statement - the first switch in this function
     * allows agent library to process event. We refrain from attaching
     * to VM then because a fault while processing events will take
     * down the whole VM!
     */
    switch (event->type) {
        case FLX_CI_NEW_OTA_IMAGE_AVAIL:
        {
            FLX_PAL_LOG_NOISE("OTA version: %s", update_version);
            jstring msgStr = cEnv.env->NewStringUTF(event->data.ota_manifest);
            cEnv.env->CallStaticVoidMethod(cEnv.cls, cEnv.cbID, event->type, msgStr);
        }
            break;
        case FLX_CI_IOTHUB_CONNECTION_DOWN:
        {
            cEnv.env->CallStaticVoidMethod(cEnv.cls, cEnv.cbID, event->type, NULL);
        }
        break;
        default:
            break;
    }

    if (manualAttach) {
        cEnv.vm->DetachCurrentThread();
    }
}

void agent::messageArrived(char *msg, char *cookie)
{
    FLX_PAL_LOG_NOISE("FLX CI message received: %s", msg);
    FLX_PAL_LOG_NOISE("Cookie: %s", cookie);

    if (!jvmIsReady()) {
        FLX_PAL_LOG_NOISE("JVM is not ready for message");
        return;
    }

    bool manualAttach = false;
    if (cEnv.vm->GetEnv((void **)&cEnv.env, JNI_VERSION_1_6) == JNI_EDETACHED) {
        manualAttach = true;
        if (cEnv.vm->AttachCurrentThread(&cEnv.env, NULL) != JNI_OK) {
            FLX_PAL_LOG_NOISE("Unable to attach to eApp service");
            return;
        }
    }

    jstring msgStr = cEnv.env->NewStringUTF(msg);
    cEnv.cbID = cEnv.env->GetStaticMethodID(cEnv.cls,"processAgentMsg",
                                            "(Ljava/lang/String;)V");
    if (!cEnv.cbID) {
        FLX_PAL_LOG_NOISE("Unable to send message to eApp service");
        return;
    }
    cEnv.env->CallStaticVoidMethod(cEnv.cls, cEnv.cbID, msgStr);

    if (manualAttach) {
        cEnv.vm->DetachCurrentThread();
    }
}

bool agent::jvmIsReady()
{
    pthread_mutex_lock(&mtx);
    bool ready = agent::jvmReady;
    pthread_mutex_unlock(&mtx);
    return ready;
}

void agent::setJvmReady(bool ready)
{
    pthread_mutex_lock(&mtx);
    agent::jvmReady = ready;
    pthread_mutex_unlock(&mtx);
}
