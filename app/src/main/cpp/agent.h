#ifndef EAPP_AGENT_H
#define EAPP_AGENT_H

extern "C" {
#include <flx-log.h>
#include <flx-ci-agent.h>
char *EApp_getOTAFileDir();
}

#include <string>
#include <jni.h>

typedef struct _nativeClientEnv {
    JavaVM *vm;
    JNIEnv *env;
    jobject obj;
    jclass cls;
    jmethodID cbID;
} nativeClientEnv;

class agent {
public:
    static const char *TAG;
    FLX_CI_Connection_Handle conn_handle;
    static pthread_mutex_t mtx;
    static bool jvmReady;
    //static nativeClientEnv *cenv;

    agent(nativeClientEnv *cenv) {

    }

    static void eventCallback(FLX_CI_Event_t *event);
    static void messageArrived(char *msg, char *cookie);
    static bool jvmIsReady();
    static void setJvmReady(bool ready);

    int init(FLX_CI_Device_Info_t *deviceInfo, char *cert);
    std::string getAgentLibVersion();
    int getActivationStatus();
    int clearActivationData();
    int activate();
    int deactivate();
    int sync();
    int connect();
    int disconnect();
    int sendAttributeMsg(uint32_t epId, uint32_t attrId,
                         FLX_CI_DataElement_t *attr_data_elem);
    int addDynamicEndpoint(FLX_CI_Multi_Ep_t *MultiEpInfoArray, int numOfEps);
    int deleteDynamicEndpoint(uint32_t epId);
};




#endif //EAPP_AGENT_H
