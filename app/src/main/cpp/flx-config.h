/*
 * flx-config.h
 *
 *  Created on: Jun 10, 2016
 *      Author: johnb
 */

#ifndef FLX_CONFIG_H_
#define FLX_CONFIG_H_

#include "flx-log.h"

/********* AGENT settings *********/

/* connection-related definitions*/
#define FLX_CI_MAX_MQTT_CONNECTIONS 1
#define FLX_CI_MAX_HTTPS_CONNECTIONS 2
#define FLX_CI_DEVICE_NAME_LEN      128
#define FLX_CI_HOST_NAME_LEN        128
#define FLX_CI_USER_NAME_LEN        128
#define FLX_CI_AUTH_TOKEN_MAX_LEN   256
#define FLX_CI_CONNECT_URI_MAX_LEN  128
#define FLX_CI_SAS_TOKEN_MAX_LEN    FLX_CI_AUTH_TOKEN_MAX_LEN
#define FLX_CI_MQTT_TOPIC_LEN       256



/* device status definitions */
#define FLX_CI_DEVICE_STATUS_NOT_ACTIVATED  "0x10"
#define FLX_CI_DEVICE_STATUS_ACTIVATED      "0x00"


/* OTA-related */
#define FLX_CI_OTA_MANIFEST_URL_LEN  512
#define FLX_CI_OTA_MANIFEST_LEN      1024
#define FLX_CI_MAX_TOKENS            4*1024
#define FLX_CI_IMG_HASH_LEN          64
#define OTA_IMG_MAX_SIZE             5*1024
#define SHA256_SIZE                  32
#define OTA_IMG_BUF_SIZE             32768
#define OTA_FILE "/sdcard"
#define OTA_UNTARED_FOLDER_PREFIX "image_"


/* endpoint/attribute related */
#define FLX_CI_ENDPOINT_INFO_MAX_LEN  1024  /* assumed max size of endpoint info in activate/sync response */
#define FLX_CI_ATTR_INFO_MAX_LEN      128   /* assumed max size of attribute info in activate/sync reponse */
#define FLX_CI_MAX_ATTRIBUTES         128   /* assumed max number of attributes */
#define FLX_CI_GUID_MAX_LEN           64    /* assumed max lenth of GUID string */
#define FLX_CI_USER_FIELD_MAX_LEN     64    /* assumed max lenth of GUID string */
#define FLX_CI_BASE64_STR_MAX_LEN    1800   /* change as need subject to system contraints */
#define FLX_CI_ENDPOINTS_PER_PAGE    10

/* field lengths */
#define FLX_CI_COMPANY_ID_MAX_LEN 64
#define FLX_CI_VIRTUAL_PRODUCT_ID_MAX_LEN 64
#define FLX_CI_PRODUCT_ID_MAX_LEN 64
#define FLX_CI_PRODUCT_LINE_ID_MAX_LEN 64
#define FLX_CI_API_KEY_LEN 			64
#define FLX_CI_COOKIE_LEN 			64
/********* MQTT default settings *********/
#define FLX_MQTT_WRITE_BUF_LEN          2048
#define FLX_MQTT_READ_BUF_LEN           4096
#define FLX_MQTT_PORT                   8883
#define FLX_MQTT_TIMEOUT_MS             1000
#define FLX_MQTT_KEEP_ALIVE_INTERVAL    60
#define FLX_MQTT_SEND_MSG_PAYLOAD_LEN   64
#define FLX_MQTT_VERSION                4
#define FLX_MQTT_URI_LEN                128
/* for multithread, give small timeslice frequently */
/* for single thread, give larger timeslice less frequently */
#define FLX_MQTT_EVENT_TIMESLICE        1
#define FLX_MQTT_MAX_MISSED_PING        3

/********* HTTP default settings *********/
#define FLX_HTTPS_PORT               443  /* TLS */
#define FLX_HTTPS_URI_LEN            128
#define FLX_HTTPS_TIMEOUT_MS         10000
#define FLX_HTTPS_BUFFER_LEN         (4*1024)
#define FLX_HTTPS_BUFFER_MAX         (64*1024)

#define FLX_TMP_URI_LEN              1024

/********* NVM settings *********/
#define FLX_PAL_NVM_COOKIE       0xA5
#define FLX_PAL_NVM_KEY_MAX_LEN  (64-1)
#define FLX_PAL_NVM_DATA_MAX_LEN (256-1)
#define FLX_PAL_NVM_FILE_MAX_LEN ((4096 * 2) - 512)
#define FLX_PAL_NVM_FILE_NAME    "/opt/agent/nvm.bin"
#define FLX_PAL_NVM_ATTR_FILE_NAME "/opt/agent/nvm_attr.bin"
#define FLX_PAL_NVM_JSON_MAX_DEPTH 8
/* increment this if the flash layout changes for endpoints/attributes. Any time the structures are modified. */
#define FLX_PAL_NVM_ATTR_FLASH_VER 0x01
#define FLX_PAL_NVM_DUMMY_ETAG "abc123"

/* package attributes in more compact manner */
#define FLX_PAL_NVM_MAX_ENDPOINTS 2
#define FLX_PAL_NVM_MAX_ATTRIBUTES 64
#define FLX_CI_NUM_BYTES_IN_COMPACT_GUID 16 /* binary encoded GUID */

/* commands-related */
#define FLX_CI_CMD_CONSUMER_AGENT  "1"
#define FLX_CI_CMD_CONSUMER_APP    "2"

/* endpoint message related */
#define FLX_CI_MAX_NUM_EP_MSGS  1  /* number of endpoint messages which can be built simultaneously */

/* size of timestamp string */
#define FLX_CI_TIMESTAMP_STR_LEN  64

/* return receipt related */
#define FLX_CI_RET_RECEIPT_FALSE  "false"
#define FLX_CI_RET_RECEIPT_TRUE   "true"

/********* RSA settings *********/
#define FLX_RSA_KEY_LEN 256 /* 2048 bit key */
// #define FLX_RSA_KEY_LEN 128 /* 1024 bit key */

#define OTA_PUBLIC_KEY_EXPONENT {0x01,0x00,0x01}
#define OTA_PUBLIC_KEY_MODULUS {                                        \
      0xb3, 0x54, 0xd1, 0x3c, 0x00, 0x22, 0x51, 0x13, 0xad, 0x7b, 0x01, 0x36, 0x86, 0xb2, 0x05, 0xb3, 0xc4, 0x67, \
      0x08, 0x7b, 0x16, 0x33, 0x1f, 0x69, 0x98, 0x9a, 0x8a, 0xef, 0x08, 0x4d, 0xba, 0xb1, 0xbe, 0x73, 0xab, 0xd4, \
      0xb5, 0xc2, 0xbe, 0x16, 0x2f, 0x2c, 0xc0, 0x29, 0x9b, 0x68, 0x3c, 0xaf, 0xb4, 0x38, 0x59, 0xb6, 0xde, 0x8b, \
      0x4c, 0x74, 0xe0, 0xd0, 0xea, 0x4f, 0xd0, 0x48, 0xbd, 0x88, 0x2c, 0x42, 0xeb, 0x31, 0x02, 0xc4, 0x3c, 0x01, \
      0xe9, 0x0f, 0xc4, 0xe1, 0xd4, 0xea, 0x7f, 0xe5, 0xfc, 0xbb, 0x48, 0x81, 0x5e, 0xda, 0x38, 0x4a, 0x18, 0xcd, \
      0x99, 0x49, 0x17, 0xe8, 0x4c, 0x8e, 0xae, 0xe9, 0x45, 0x28, 0x14, 0x9f, 0xfc, 0xfe, 0xd7, 0xe0, 0x53, 0x76, \
      0xc9, 0x3d, 0x48, 0xe3, 0x96, 0x2f, 0x17, 0x0a, 0x72, 0x23, 0xa1, 0x41, 0xf5, 0x85, 0xd8, 0x82, 0x8a, 0x51, \
      0xe0, 0x2e, 0xdd, 0xfe, 0x12, 0x8b, 0xdf, 0x96, 0x60, 0x66, 0x73, 0xfa, 0x9e, 0x22, 0x62, 0x1c, 0x6c, 0x7e, \
      0xf1, 0x66, 0x09, 0x6a, 0x9f, 0xce, 0xa1, 0x73, 0xb2, 0x63, 0x78, 0xcd, 0xaf, 0xd2, 0xac, 0x1e, 0xf5, 0xfd, \
      0xdd, 0x48, 0x4e, 0x73, 0x28, 0x0b, 0x84, 0x56, 0x16, 0x83, 0x03, 0xcb, 0x1d, 0x21, 0x59, 0x64, 0xdc, 0xff, \
      0xaf, 0x3e, 0xd7, 0xf8, 0xc6, 0x17, 0xc6, 0x72, 0xd6, 0xc2, 0x6c, 0xda, 0xad, 0xc3, 0xe8, 0x22, 0x20, 0x1d, \
      0x80, 0x76, 0x3c, 0x3b, 0x04, 0xac, 0x14, 0xfc, 0xcd, 0x19, 0x2a, 0x46, 0x39, 0xd2, 0x52, 0xbf, 0xb7, 0xbf, \
      0xbc, 0xe6, 0x29, 0x4c, 0xec, 0xbf, 0xf0, 0xec, 0x5d, 0x35, 0x7c, 0x33, 0x1e, 0xf5, 0x4c, 0xd4, 0xfe, 0x1c, \
      0x96, 0x3c, 0x81, 0x87, 0x1c, 0x68, 0xfa, 0x02, 0x1c, 0x1f, 0xd4, 0x91, 0xf5, 0xa2, 0x1b, 0x4f, 0xef, 0x73, \
      0xe6, 0x3f, 0xad, 0x41                                            \
   }

/* some platforms do not have all string comparator funcitions */
//#define FLX_CI_NEED_STRNCASECMP

/********* DATABASE **********/
#define DB_NAME "agent.db"
#endif /*FLX_CONFIG_H*/
